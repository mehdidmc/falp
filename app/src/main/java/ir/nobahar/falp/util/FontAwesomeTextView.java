package ir.nobahar.falp.util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

public class FontAwesomeTextView extends AppCompatTextView {

    public FontAwesomeTextView(@NonNull Context context) {
        super(context);
        init();
    }

    public FontAwesomeTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FontAwesomeTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "font_awesome.ttf");
        setTypeface(typeface);
    }
}
