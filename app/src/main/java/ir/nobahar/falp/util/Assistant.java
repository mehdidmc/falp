package ir.nobahar.falp.util;

import android.content.Context;
import android.os.Build;
import android.util.Base64;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.nio.charset.StandardCharsets;

import ir.nobahar.falp.R;

public class Assistant {

    public static final String DEVICE_KEY = "3fc96867c85a";
    public static final int LAUNCH_FILTER_ACTIVITY = 2001;

    private static Assistant instance = null;

    private Assistant(){}

    public static synchronized Assistant get() {
        if (instance == null) instance = new Assistant();
        return instance;
    }

    public void setFragmentToFrameNoAnimation(Fragment fragment, FragmentManager fragmentManager, int resourceId) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(resourceId, fragment);
        transaction.commit();
    }

    public void setFragmentToLayoutWithAnimation(Fragment fragment, FragmentManager fragmentManager, int resourceId) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.fragment_enter_anim, R.anim.fragment_exit_anim, R.anim.fragment_pop_enter_anim, R.anim.fragment_pop_exit_anim);
        transaction.replace(resourceId, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public String decodeJwt(String encodedJwt) {
        String[] splitJwt = encodedJwt.split("\\.");
        byte[] decodedJwt = Base64.decode(splitJwt[1], Base64.URL_SAFE);
        return new String(decodedJwt, StandardCharsets.UTF_8);
    }

    public String getDeviceName() {
        return Build.PRODUCT + " " + Build.MODEL;
    }

    public String setFontAwesome(String unicode) {
        return new String(Character.toChars(Integer.parseInt(unicode, 16)));
    }

    public String getUserId(Context context) {
        String jwt = JwtTokens.getInstance(context).getJwtTokens();
        String decodedJwt = decodeJwt(jwt);
        JsonObject jsonObject = new Gson().fromJson(decodedJwt, JsonObject.class);
        return jsonObject.getAsJsonObject("data").get("id").getAsString();
    }
}
