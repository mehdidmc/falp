package ir.nobahar.falp.util;

import android.content.Context;
import android.content.SharedPreferences;

public class JwtTokens {

    private static final String JWT_TOKENS = "jwt_tokens";

    private static JwtTokens instance = null;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private JwtTokens(Context context) {
        preferences = context.getSharedPreferences(JWT_TOKENS, Context.MODE_PRIVATE);
        editor = context.getSharedPreferences(JWT_TOKENS, Context.MODE_PRIVATE).edit();
    }

    public static synchronized JwtTokens getInstance(Context context) {
        if (instance == null) instance = new JwtTokens(context);
        return instance;
    }

    public void setJwtTokens(String tokens) {
        editor.putString("tokens", tokens);
        editor.apply();
    }

    public String getJwtTokens() {
        return preferences.getString("tokens", "no_value");
    }
}
