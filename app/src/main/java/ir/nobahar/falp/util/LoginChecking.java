package ir.nobahar.falp.util;

import android.content.Context;
import android.content.SharedPreferences;

public class LoginChecking {

    public static final String LOGIN_CHECKING = "login_checking";

    private static LoginChecking instance = null;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private LoginChecking(Context context) {
        preferences = context.getSharedPreferences(LOGIN_CHECKING, Context.MODE_PRIVATE);
        editor = context.getSharedPreferences(LOGIN_CHECKING, Context.MODE_PRIVATE).edit();
    }

    public static synchronized LoginChecking getInstance(Context context) {
        if (instance == null) instance = new LoginChecking(context);
        return instance;
    }

    public void setLoginMode(boolean mode) {
        editor.putBoolean("login_mode", mode);
        editor.commit();
    }

    public boolean getLoginMode() {
        return preferences.getBoolean("login_mode", false);
    }
}
