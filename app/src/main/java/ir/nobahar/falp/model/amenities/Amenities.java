package ir.nobahar.falp.model.amenities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Amenities {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("category_id")
    private String categoryId;

    @Expose
    @SerializedName("amenities_id")
    private String amenitiesId;

    @Expose
    @SerializedName("amenities_title")
    private String amenitiesTitle;

    @Expose
    @SerializedName("icon")
    private String icon;

    @Expose
    @SerializedName("multi_tasking")
    private String multiTasking;

    @Expose
    @SerializedName("url")
    private String url;

    @Expose
    @SerializedName("amounts")
    private List<AmenitiesChild> children;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getAmenitiesId() {
        return amenitiesId;
    }

    public void setAmenitiesId(String amenitiesId) {
        this.amenitiesId = amenitiesId;
    }

    public String getAmenitiesTitle() {
        return amenitiesTitle;
    }

    public void setAmenitiesTitle(String amenitiesTitle) {
        this.amenitiesTitle = amenitiesTitle;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMultiTasking() {
        return multiTasking;
    }

    public void setMultiTasking(String multiTasking) {
        this.multiTasking = multiTasking;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<AmenitiesChild> getChildren() {
        return children;
    }

    public void setChildren(List<AmenitiesChild> children) {
        this.children = children;
    }
}
