package ir.nobahar.falp.model.business;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessCategory implements Serializable {

    @Expose
    @SerializedName("category_id")
    private String categoryId = "";

    @Expose
    @SerializedName("business_id")
    private String businessId = "";

    @Expose
    @SerializedName("title")
    private String title = "";

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
