package ir.nobahar.falp.model.scoreDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ScoreDetail {

    @Expose
    @SerializedName("score")
    private String score;

    @Expose
    @SerializedName("score_count")
    private String scoreCount;

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getScoreCount() {
        return scoreCount;
    }

    public void setScoreCount(String scoreCount) {
        this.scoreCount = scoreCount;
    }
}
