package ir.nobahar.falp.model.businessSearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ir.nobahar.falp.model.business.Business;

public class BusinessSearchResponse {

    @Expose
    @SerializedName("data")
    private List<Business> businesses;

    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("pages")
    private String pages;

    public List<Business> getBusinesses() {
        return businesses;
    }

    public void setBusinesses(List<Business> businesses) {
        this.businesses = businesses;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }
}
