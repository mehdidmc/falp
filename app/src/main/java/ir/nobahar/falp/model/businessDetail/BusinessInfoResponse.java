package ir.nobahar.falp.model.businessDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ir.nobahar.falp.model.business.Business;

public class BusinessInfoResponse {

    @Expose
    @SerializedName("data")
    private List<Business> businesses;

    public List<Business> getBusinesses() {
        return businesses;
    }

    public void setBusinesses(List<Business> businesses) {
        this.businesses = businesses;
    }
}
