package ir.nobahar.falp.model.businessDetail.BusinessInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BusinessAmenities implements Serializable {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("amenities_id")
    private String amenitiesId;

    @Expose
    @SerializedName("amenities_title")
    private String title;

    @Expose
    @SerializedName("icon")
    private String icon;

    @Expose
    @SerializedName("unicode")
    private String unicode;

    @Expose
    @SerializedName("amount")
    private List<String> amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmenitiesId() {
        return amenitiesId;
    }

    public void setAmenitiesId(String amenitiesId) {
        this.amenitiesId = amenitiesId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUnicode() {
        return unicode;
    }

    public void setUnicode(String unicode) {
        this.unicode = unicode;
    }

    public List<String> getAmount() {
        return amount;
    }

    public void setAmount(List<String> amount) {
        this.amount = amount;
    }
}
