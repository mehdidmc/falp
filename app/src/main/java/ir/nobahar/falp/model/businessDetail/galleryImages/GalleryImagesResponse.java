package ir.nobahar.falp.model.businessDetail.galleryImages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GalleryImagesResponse {

    @Expose
    @SerializedName("data")
    private List<GalleryImages> images;

    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("number_gallery")
    private String numberGallery;

    @Expose
    @SerializedName("pages")
    private String pages;

    public List<GalleryImages> getImages() {
        return images;
    }

    public void setImages(List<GalleryImages> images) {
        this.images = images;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNumberGallery() {
        return numberGallery;
    }

    public void setNumberGallery(String numberGallery) {
        this.numberGallery = numberGallery;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }
}
