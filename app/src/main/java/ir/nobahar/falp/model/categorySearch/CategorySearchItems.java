package ir.nobahar.falp.model.categorySearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategorySearchItems {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("title")
    private String title;

    @Expose
    @SerializedName("url")
    private String url;

    @Expose
    @SerializedName("parent")
    private List<CategorySearchParent> parent;

    @Expose
    @SerializedName("child")
    private List<CategorySearchChild> child;

    @Expose
    @SerializedName("icon")
    private String icon;

    @Expose
    @SerializedName("for")
    private String forUse;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<CategorySearchParent> getParent() {
        return parent;
    }

    public void setParent(List<CategorySearchParent> parent) {
        this.parent = parent;
    }

    public List<CategorySearchChild> getChild() {
        return child;
    }

    public void setChild(List<CategorySearchChild> child) {
        this.child = child;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getForUse() {
        return forUse;
    }

    public void setForUse(String forUse) {
        this.forUse = forUse;
    }
}
