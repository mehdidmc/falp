package ir.nobahar.falp.model.business;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessWorkHours implements Serializable {

    @Expose
    @SerializedName("work_day")
    private String dayName;

    @Expose
    @SerializedName("opening_time")
    private String openingTime;

    @Expose
    @SerializedName("closing_time")
    private String closeTime;

    @Expose
    @SerializedName("closed")
    private String isClosed;

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(String isClosed) {
        this.isClosed = isClosed;
    }
}
