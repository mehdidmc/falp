package ir.nobahar.falp.model.business;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import ir.nobahar.falp.model.businessDetail.BusinessInfo.BusinessAmenities;

public class Business implements Serializable {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("user_id")
    private String userId;

    @Expose
    @SerializedName("biz_id")
    private String bizId;

    @Expose
    @SerializedName("biz_hash_id")
    private String bizHashId;

    @Expose
    @SerializedName("hash_id")
    private String hashId;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("url")
    private String url;

    @Expose
    @SerializedName("country_id")
    private String countryId;

    @Expose
    @SerializedName("city_id")
    private String cityId;

    @Expose
    @SerializedName("address1")
    private String address1;

    @Expose
    @SerializedName("address2")
    private String address2;

    @Expose
    @SerializedName("longitude")
    private String longitude;

    @Expose
    @SerializedName("latitude")
    private String latitude;

    @Expose
    @SerializedName("phone")
    private String phone;

    @Expose
    @SerializedName("mobile")
    private String mobile;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("website")
    private String website;


    @Expose
    @SerializedName("instagram_id")
    private String instagramId;


    @Expose
    @SerializedName("telegram_id")
    private String telegramId;

    @Expose
    @SerializedName("price")
    private String price;

    @Expose
    @SerializedName("commetnsNumber")
    private String commentsCount;

    @Expose
    @SerializedName("commetnsScore")
    private String commentsScoreCounts;

    @Expose
    @SerializedName("active1")
    private String active1;

    @Expose
    @SerializedName("active2")
    private String active2;

    @Expose
    @SerializedName("open_now")
    private boolean isOpen;

    @Expose
    @SerializedName("details_open")
    private String openDetail;

    @Expose
    @SerializedName("categoryBusiness")
    private List<BusinessCategory> categories;

    @Expose
    @SerializedName("gallery")
    private List<BusinessGallery> galleries;

    @Expose
    @SerializedName("comments")
    private List<BusinessComments> comments;

    @Expose
    @SerializedName("hours")
    private List<BusinessWorkHours> workHours;

    @Expose
    @SerializedName("amountAmenitiesBusiness")
    private List<BusinessAmenities> amenities;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBizHashId() {
        return bizHashId;
    }

    public void setBizHashId(String bizHashId) {
        this.bizHashId = bizHashId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBizId() {
        return bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(String commentsCount) {
        this.commentsCount = commentsCount;
    }

    public String getCommentsScoreCounts() {
        return commentsScoreCounts;
    }

    public void setCommentsScoreCounts(String commentsScoreCounts) {
        this.commentsScoreCounts = commentsScoreCounts;
    }

    public String getActive1() {
        return active1;
    }

    public void setActive1(String active1) {
        this.active1 = active1;
    }

    public List<BusinessCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<BusinessCategory> categories) {
        this.categories = categories;
    }

    public List<BusinessGallery> getGalleries() {
        return galleries;
    }

    public void setGalleries(List<BusinessGallery> galleries) {
        this.galleries = galleries;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getActive2() {
        return active2;
    }

    public void setActive2(String active2) {
        this.active2 = active2;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public List<BusinessComments> getComments() {
        return comments;
    }

    public void setComments(List<BusinessComments> comments) {
        this.comments = comments;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getInstagramId() {
        return instagramId;
    }

    public void setInstagramId(String instagramId) {
        this.instagramId = instagramId;
    }

    public String getTelegramId() {
        return telegramId;
    }

    public void setTelegramId(String telegramId) {
        this.telegramId = telegramId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public List<BusinessWorkHours> getWorkHours() {
        return workHours;
    }

    public void setWorkHours(List<BusinessWorkHours> workHours) {
        this.workHours = workHours;
    }

    public List<BusinessAmenities> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<BusinessAmenities> amenities) {
        this.amenities = amenities;
    }

    public String getOpenDetail() {
        return openDetail;
    }

    public void setOpenDetail(String openDetail) {
        this.openDetail = openDetail;
    }
}
