package ir.nobahar.falp.model.businessDetail.fullView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryImages;

public class ImportantImagesResponse {

    @Expose
    @SerializedName("data")
    private List<GalleryImages> images;

    public List<GalleryImages> getImages() {
        return images;
    }

    public void setImages(List<GalleryImages> images) {
        this.images = images;
    }
}
