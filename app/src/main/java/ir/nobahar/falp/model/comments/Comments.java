package ir.nobahar.falp.model.comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ir.nobahar.falp.model.user.User;

public class Comments {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("score")
    private String score;

    @Expose
    @SerializedName("comments")
    private String comment;

    @Expose
    @SerializedName("created_at")
    private String createdAt;

    @Expose
    @SerializedName("business_id")
    private String businessId;

    @Expose
    @SerializedName("admin_id")
    private String adminId;

    @Expose
    @SerializedName("parent_id")
    private String parentId;

    @Expose
    @SerializedName("active")
    private String active;

    @Expose
    @SerializedName("number_comment_images")
    private String commentCounts;

    @Expose
    @SerializedName("feedback1")
    private String feedback1;

    @Expose
    @SerializedName("feedback2")
    private String feedback2;

    @Expose
    @SerializedName("feedback3")
    private String feedback3;

    @Expose
    @SerializedName("user")
    private List<User> user;

    @Expose
    @SerializedName("gallery_item")
    private List<CommentImages> commentImages;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCommentCounts() {
        return commentCounts;
    }

    public void setCommentCounts(String commentCounts) {
        this.commentCounts = commentCounts;
    }

    public String getFeedback1() {
        return feedback1;
    }

    public void setFeedback1(String feedback1) {
        this.feedback1 = feedback1;
    }

    public String getFeedback2() {
        return feedback2;
    }

    public void setFeedback2(String feedback2) {
        this.feedback2 = feedback2;
    }

    public String getFeedback3() {
        return feedback3;
    }

    public void setFeedback3(String feedback3) {
        this.feedback3 = feedback3;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }

    public List<CommentImages> getCommentImages() {
        return commentImages;
    }

    public void setCommentImages(List<CommentImages> commentImages) {
        this.commentImages = commentImages;
    }
}
