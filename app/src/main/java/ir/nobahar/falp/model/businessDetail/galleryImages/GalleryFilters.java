package ir.nobahar.falp.model.businessDetail.galleryImages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GalleryFilters {

    @Expose
    @SerializedName("cat_id")
    private String categoryId;

    @Expose
    @SerializedName("cat_title")
    private String categoryTitle;

    @Expose
    @SerializedName("number_image")
    private String imageNumber;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getImageNumber() {
        return imageNumber;
    }

    public void setImageNumber(String imageNumber) {
        this.imageNumber = imageNumber;
    }
}
