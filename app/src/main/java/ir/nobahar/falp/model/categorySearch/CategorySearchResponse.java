package ir.nobahar.falp.model.categorySearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategorySearchResponse {

    @Expose
    @SerializedName("data")
    private List<CategorySearchItems> items;

    @Expose
    @SerializedName("status")
    private String status;

    public List<CategorySearchItems> getItems() {
        return items;
    }

    public void setItems(List<CategorySearchItems> items) {
        this.items = items;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
