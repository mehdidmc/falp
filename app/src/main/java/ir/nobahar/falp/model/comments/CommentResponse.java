package ir.nobahar.falp.model.comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.w3c.dom.Comment;

import java.util.List;

public class CommentResponse {

    @Expose
    @SerializedName("data")
    private List<Comments> comments;

    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("number_comments")
    private String commentCount;

    @Expose
    @SerializedName("pages")
    private String pages;

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }
}
