package ir.nobahar.falp.model.amenities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AmenitiesResponse {

    @Expose
    @SerializedName("show_category")
    private boolean showCategory;

    @Expose
    @SerializedName("category_child")
    private String categoryChild;

    @Expose
    @SerializedName("top_categories")
    private String topCategories;

    @Expose
    @SerializedName("categories")
    private String categories;

    @Expose
    @SerializedName("amenities")
    private List<Amenities> amenities;

    @Expose
    @SerializedName("top_amenities")
    private List<TopAmenities> topAmenities;

    public boolean isShowCategory() {
        return showCategory;
    }

    public void setShowCategory(boolean showCategory) {
        this.showCategory = showCategory;
    }

    public String getCategoryChild() {
        return categoryChild;
    }

    public void setCategoryChild(String categoryChild) {
        this.categoryChild = categoryChild;
    }

    public String getTopCategories() {
        return topCategories;
    }

    public void setTopCategories(String topCategories) {
        this.topCategories = topCategories;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public List<Amenities> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<Amenities> amenities) {
        this.amenities = amenities;
    }

    public List<TopAmenities> getTopAmenities() {
        return topAmenities;
    }

    public void setTopAmenities(List<TopAmenities> topAmenities) {
        this.topAmenities = topAmenities;
    }
}
