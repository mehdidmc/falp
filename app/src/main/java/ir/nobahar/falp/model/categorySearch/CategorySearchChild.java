package ir.nobahar.falp.model.categorySearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategorySearchChild {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("title")
    private String title;

    @Expose
    @SerializedName("parent")
    private String parent;

    @Expose
    @SerializedName("for")
    private String forUse;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getForUse() {
        return forUse;
    }

    public void setForUse(String forUse) {
        this.forUse = forUse;
    }
}
