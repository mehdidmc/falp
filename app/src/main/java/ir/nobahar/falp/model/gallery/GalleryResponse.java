package ir.nobahar.falp.model.gallery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GalleryResponse {

    @Expose
    @SerializedName("pages")
    private String pages;

    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("number_gallery")
    private String galleryCount;

    @Expose
    @SerializedName("data")
    private List<Gallery> galleries;

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGalleryCount() {
        return galleryCount;
    }

    public void setGalleryCount(String galleryCount) {
        this.galleryCount = galleryCount;
    }

    public List<Gallery> getGalleries() {
        return galleries;
    }

    public void setGalleries(List<Gallery> galleries) {
        this.galleries = galleries;
    }
}
