package ir.nobahar.falp.model.businessSearch;

import androidx.annotation.Nullable;

import java.io.Serializable;

import ir.nobahar.falp.view.activities.search.SearchView;

public class SearchModel implements Serializable {

    public static final double DEFAULT_LAT = 35.698406;
    public static final double DEFAULT_LNG = 51.389632;

    public static final double X_ZOOM_12 = 0.05872;
    public static final double Y_ZOOM_12 = 0.06843;

    public static final double X_ZOOM_13 = 0.030602;
    public static final double Y_ZOOM_13 = 0.033637;

    public static final double X_ZOOM_14 = 0.015563;
    public static final double Y_ZOOM_14 = 0.016901;

    public static final double X_ZOOM_15 = 0.008076;
    public static final double Y_ZOOM_15 = 0.008478;

    public static final double X_ZOOM_16 = 0.004087;
    public static final double Y_ZOOM_16 = 0.004205;

    private String start;
    private String count;
    private String findDesc;
    private String minLat;
    private String maxLat;
    private String minLng;
    private String maxLng;
    private String userId;
    private String attrs;
    private String openNow = "0";
    private String price = "";
    private String sort = "0";
    private String ctgs;
    private String cityId;
    private int filterCount = 0;

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getFindDesc() {
        return findDesc;
    }

    public void setFindDesc(String findDesc) {
        this.findDesc = findDesc;
    }

    public String getMinLat() {
        return minLat;
    }

    public void setMinLat(String minLat) {
        this.minLat = minLat;
    }

    public String getMaxLat() {
        return maxLat;
    }

    public void setMaxLat(String maxLat) {
        this.maxLat = maxLat;
    }

    public String getMinLng() {
        return minLng;
    }

    public void setMinLng(String minLng) {
        this.minLng = minLng;
    }

    public String getMaxLng() {
        return maxLng;
    }

    public void setMaxLng(String maxLng) {
        this.maxLng = maxLng;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAttrs() {
        return attrs;
    }

    public void setAttrs(String attrs) {
        this.attrs = attrs;
    }

    public String getOpenNow() {
        return openNow;
    }

    public void setOpenNow(String openNow) {
        this.openNow = openNow;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getCtgs() {
        return ctgs;
    }

    public void setCtgs(String ctgs) {
        this.ctgs = ctgs;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public int getFilterCount() {
        return filterCount;
    }

    public void setFilterCount(int filterCount, @Nullable SearchView view) {
        this.filterCount = Math.max(filterCount, 0);
        if (view != null) view.onFilterNumberChanged(this.filterCount);
    }
}
