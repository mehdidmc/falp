package ir.nobahar.falp.model.amenities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AmenitiesChild {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("title")
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
