package ir.nobahar.falp.webServices.services;

import java.util.List;

import ir.nobahar.falp.model.businessDetail.BusinessInfoResponse;
import ir.nobahar.falp.model.businessDetail.fullView.ImportantImagesResponse;
import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryFilters;
import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryImagesResponse;
import ir.nobahar.falp.model.comments.CommentResponse;
import ir.nobahar.falp.model.scoreDetail.ScoreDetail;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface BusinessDetailServices {

    @GET("gallery/category")
    Call<List<GalleryFilters>> getGalleryFilters(
            @Query("business_id") String businessID,
            @Query("users_visit_id") String userVisitedID );

    @GET("gallery/read")
    Call<GalleryImagesResponse> getGalleryImages(
            @Query("business_id") String businessId,
            @Query("category_id") String categoryId,
            @Query("start") String start,
            @Query("count") String count );

    @GET("business/info")
    Call<BusinessInfoResponse> getBusinessInfo(
            @Header("Authorization") String jwt,
            @Query("url") String url,
            @Query("user_id") String userId,
            @Query("biz_id") String bizId );

    @GET("business/ratesDetails")
    Call<List<ScoreDetail>> getScoreDetail(
            @Query("url") String url );

    @GET("gallery/business")
    Call<ImportantImagesResponse> getImportantImages(
            @Query("business_id") String businessId,
            @Query("users_visit_id") String userVisitedId );

    @GET("comments/read")
    Call<CommentResponse> getComments(
            @Query("url") String url,
            @Query("user_id") String userId,
            @Query("start") String startPage,
            @Query("count") String count );
}
