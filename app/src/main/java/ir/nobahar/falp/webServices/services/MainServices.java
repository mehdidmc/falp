package ir.nobahar.falp.webServices.services;

import com.google.gson.JsonObject;

import ir.nobahar.falp.model.businessSaved.BusinessSavedResponse;
import ir.nobahar.falp.model.gallery.GalleryResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface MainServices {

    @GET("pages/home/slide")
    Call<JsonObject> sliderImages(@Query("city_id") String cityId,
                                  @Query("user_id") String userId);

    @GET("businessSave/user")
    Call<BusinessSavedResponse> getSavedBusiness(@Header("Authorization") String jwt,
                                                 @Query("user_id") String hashId,
                                                 @Query("start") int start,
                                                 @Query("count") int count);

    @GET("gallery/userDetails")
    Call<GalleryResponse> getUserGalleries(@Query("user_id") String hashId,
                                           @Query("start") int start,
                                           @Query("count") int count);
}
