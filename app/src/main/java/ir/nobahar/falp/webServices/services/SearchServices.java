package ir.nobahar.falp.webServices.services;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.Nullable;

import ir.nobahar.falp.model.amenities.AmenitiesResponse;
import ir.nobahar.falp.model.categorySearch.CategorySearchResponse;
import ir.nobahar.falp.model.businessSearch.BusinessSearchResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SearchServices {

    @GET("business/search")
    Call<BusinessSearchResponse> searchByLocation(@NonNull  @Query("start")     String start,
                                                  @NonNull  @Query("count")     String count,
                                                  @NonNull  @Query("min_lat")   String minLat,
                                                  @NonNull  @Query("max_lat")   String maxLat,
                                                  @NonNull  @Query("min_lng")   String minLng,
                                                  @NonNull  @Query("max_lng")   String maxLng,
                                                  @Nullable @Query("sort")      String sort,
                                                  @Nullable @Query("price")     String price,
                                                  @Nullable @Query("ctgs")      String categories,
                                                  @Nullable @Query("open_now")  String openNow,
                                                  @Nullable @Query("attrs")     String attrs,
                                                  @Nullable @Query("user_id")   String userId,
                                                  @Nullable @Query("find_desc") String findDesc,
                                                  @NonNull  @Query("city_id")   String cityId);

    @GET("pages/search")
    Call<AmenitiesResponse> getAmenities(@Nullable @Query("ctgs")      String categories,
                                         @NonNull  @Query("city_id")   String cityId,
                                         @Nullable @Query("user_id")   String userId,
                                         @Nullable @Query("find_desc") String findDesc);

    @GET("category/search")
    Call<CategorySearchResponse> searchByCategory(@NonNull @Query("title") String searchTitle,
                                                  @NonNull @Query("city_id") String cityId);
}
