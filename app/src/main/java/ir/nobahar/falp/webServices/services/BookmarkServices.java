package ir.nobahar.falp.webServices.services;

import com.google.gson.JsonObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface BookmarkServices {

    @POST("businessSave/create")
    Call<JsonObject> setBookmark(@Header("authorization") String jwt,
                                 @Body HashMap<String, String> body);

    @GET("businessSave/check")
    Call<JsonObject> checkBookmark(@Header("authorization") String jwt,
                                   @Query("user_id") String userId,
                                   @Query("business_id") String businessId);
}
