package ir.nobahar.falp.webServices.services;

import com.google.gson.JsonObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface SendCommentServices {

    @POST("comments/create")
    Call<JsonObject> sendComment(@Header ("authorization") String jwt,
                                 @Body HashMap<String, String> body);

    @Multipart
    @POST("gallery/upload")
    Call<ResponseBody> sendImages(@Header("authorization") String jwt,
                                  @Part MultipartBody.Part image,
                                  @Part("user_id") RequestBody userId,
                                  @Part("business_id") RequestBody businessId,
                                  @Part("comments_id") RequestBody commentId,
                                  @Part("category_gallery_id") RequestBody galleryId);
}
