package ir.nobahar.falp.webServices.services;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface SendImageServices {

    @Multipart
    @POST("gallery/upload")
    Call<ResponseBody> uploadImage(@Header("Authorization") String jwt,
                                   @Part MultipartBody.Part image,
                                   @Part("caption") RequestBody caption,
                                   @Part("user_id") RequestBody userId,
                                   @Part("business_id") RequestBody businessID,
                                   @Part("category_gallery_id") RequestBody galleryId);
}
