package ir.nobahar.falp.webServices.services;

import com.google.gson.JsonObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface LoginServices {

    @POST("auth/AppCheckPhone")
    Call<JsonObject> checkPhoneNumber(@Body HashMap<String, String> body);

    @POST("auth/AppActiveCode")
    Call<JsonObject> activeCode(@Body HashMap<String, String> body);

    @POST("auth/sendData")
    Call<JsonObject> sendUserData(@Body HashMap<String, String> body);
}
