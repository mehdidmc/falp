package ir.nobahar.falp.webServices.services;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface CommonServices {

    @Multipart
    @POST("user/avatar")
    Call<ResponseBody> uploadAvatar(@Header("Authorization") String jwt,
                                    @Part MultipartBody.Part avatar);
}
