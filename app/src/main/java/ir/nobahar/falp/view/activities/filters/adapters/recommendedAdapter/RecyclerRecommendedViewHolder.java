package ir.nobahar.falp.view.activities.filters.adapters.recommendedAdapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import ir.nobahar.falp.databinding.RecommendedItemsLayoutBinding;

public class RecyclerRecommendedViewHolder extends RecyclerView.ViewHolder {

    public RecommendedItemsLayoutBinding bind;

    public RecyclerRecommendedViewHolder(@NonNull @NotNull RecommendedItemsLayoutBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
