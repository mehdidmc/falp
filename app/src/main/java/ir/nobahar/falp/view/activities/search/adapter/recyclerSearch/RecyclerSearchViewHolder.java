package ir.nobahar.falp.view.activities.search.adapter.recyclerSearch;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.nobahar.falp.databinding.SearchResultItemsBinding;

public class RecyclerSearchViewHolder extends RecyclerView.ViewHolder {

    public SearchResultItemsBinding binding;

    public RecyclerSearchViewHolder(@NonNull SearchResultItemsBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
