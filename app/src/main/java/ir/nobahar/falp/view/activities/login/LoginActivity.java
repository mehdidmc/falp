package ir.nobahar.falp.view.activities.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.login.fragments.loginPhone.LoginPhoneFragment;
import ir.nobahar.falp.util.Assistant;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setContentToLayout();
    }

    private void setContentToLayout() {
        LoginPhoneFragment fragment = new LoginPhoneFragment();
        Assistant.get().setFragmentToFrameNoAnimation(fragment, getSupportFragmentManager(), R.id.frameLayout_loginActivity);
    }
}