package ir.nobahar.falp.view.activities.main.fragments.moreFragment;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import ir.nobahar.falp.view.activities.userAccount.UserAccountActivity;

public class MainMoreClickHandler {

    private final Context context;

    public MainMoreClickHandler(Context context) {
        this.context = context;
    }

    public void onCLickUserAccount(View view) {
        Intent settingsIntent = new Intent(context, UserAccountActivity.class);
        context.startActivity(settingsIntent);
    }
}
