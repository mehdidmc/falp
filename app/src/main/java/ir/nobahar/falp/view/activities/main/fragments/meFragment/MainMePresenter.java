package ir.nobahar.falp.view.activities.main.fragments.meFragment;

import androidx.fragment.app.FragmentActivity;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import ir.nobahar.falp.util.Assistant;
import ir.nobahar.falp.util.JwtTokens;

public class MainMePresenter {

    private final FragmentActivity activity;
    private final MainMeView view;

    public MainMePresenter(FragmentActivity activity, MainMeView view) {
        this.activity = activity;
        this.view = view;
    }

    public void setupUserInformation() {
        String jwt = JwtTokens.getInstance(activity).getJwtTokens();
        String decodedJwt = Assistant.get().decodeJwt(jwt);
        JsonObject jsonObject = new Gson().fromJson(decodedJwt, JsonObject.class);

        String name = jsonObject.getAsJsonObject("data").get("first_name").getAsString();
        String family = jsonObject.getAsJsonObject("data").get("last_name").getAsString();
        String gallery = jsonObject.getAsJsonObject("data").get("gallery_number").getAsString();
        String comments = jsonObject.getAsJsonObject("data").get("comments_number").getAsString();

        view.showUserInformation(name + " " + family, gallery, comments);
    }
}
