package ir.nobahar.falp.view.activities.main.fragments.meNotRegisteredFragment;

import android.content.Intent;
import android.view.View;

import androidx.fragment.app.FragmentActivity;

import ir.nobahar.falp.view.activities.login.LoginActivity;

public class MainMeNotRegisteredClickHandler {

    private final FragmentActivity activity;

    public MainMeNotRegisteredClickHandler(FragmentActivity activity) {
        this.activity = activity;
    }

    public void onCLickRegisterBtn(View view) {
        Intent loginIntent = new Intent(activity, LoginActivity.class);
        activity.startActivity(loginIntent);
    }
}
