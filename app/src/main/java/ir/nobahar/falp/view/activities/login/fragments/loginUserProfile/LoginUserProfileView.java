package ir.nobahar.falp.view.activities.login.fragments.loginUserProfile;

import ir.nobahar.falp.model.user.User;

public interface LoginUserProfileView {

    void showMessage(String message);

    User checkInputValidation();

    boolean avatarImageValidation();
}
