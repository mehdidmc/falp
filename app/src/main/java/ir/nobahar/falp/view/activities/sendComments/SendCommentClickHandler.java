package ir.nobahar.falp.view.activities.sendComments;

import android.view.View;

public interface SendCommentClickHandler {

    void onClickRateStar(View view);

    void onClickPickImage(View view);

    void onClickBack(View view);

    void onClickSendComment(View view);
}
