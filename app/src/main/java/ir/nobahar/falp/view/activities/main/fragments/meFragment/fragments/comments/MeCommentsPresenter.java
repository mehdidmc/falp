package ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.comments;

import androidx.fragment.app.FragmentActivity;

public class MeCommentsPresenter {

    private final MeCommentsView view;
    private final FragmentActivity activity;

    public MeCommentsPresenter(MeCommentsView view, FragmentActivity activity) {
        this.view = view;
        this.activity = activity;
    }

    public int updateRecyclerViewItems(int lastStartItem) {
        int currentStartItem = lastStartItem + 5;
        //showComments(currentStartItem);
        return currentStartItem;
    }
}













