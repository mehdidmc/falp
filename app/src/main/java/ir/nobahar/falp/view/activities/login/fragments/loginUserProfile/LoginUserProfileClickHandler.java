package ir.nobahar.falp.view.activities.login.fragments.loginUserProfile;

import android.net.Uri;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

public class LoginUserProfileClickHandler {

    private final LoginUserProfilePresenter presenter;
    private final FragmentActivity activity;
    private final LoginUserProfileFragment fragment;

    private Uri avatar;

    public LoginUserProfileClickHandler(LoginUserProfilePresenter presenter, FragmentActivity activity, LoginUserProfileFragment fragment) {
        this.presenter = presenter;
        this.activity = activity;
        this.fragment = fragment;
    }

    public void onCLickPickImage(View view) {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setAutoZoomEnabled(false)
                .start(activity, fragment);
    }

    public void onCLickBack(View view) {
        activity.getSupportFragmentManager().popBackStack();
    }

    public void onCLickConfirmBtn(View view) {
        presenter.sendUserData(avatar);
        Toast.makeText(activity, "Clicked", Toast.LENGTH_SHORT).show();
    }

    public void setAvatar(Uri avatar) {
        this.avatar = avatar;
    }
}
