package ir.nobahar.falp.view.activities.login.fragments.loginPhone;

import android.view.View;

public class LoginPhoneClickHandler {

    private final LoginPhonePresenter presenter;

    public LoginPhoneClickHandler(LoginPhonePresenter presenter) {
        this.presenter = presenter;
    }

    public void onCLickConfirmPhone(View view) {
        presenter.checkPhoneNumber();
    }
}
