package ir.nobahar.falp.view.activities.main.fragments.homeFragment;

public interface MainHomeView {

    void showMessage(String message);

    void setDefaultSliderImages();

    void setSlideUrlImage(String url);
}
