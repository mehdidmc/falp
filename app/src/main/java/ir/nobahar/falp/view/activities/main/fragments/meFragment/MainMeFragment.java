package ir.nobahar.falp.view.activities.main.fragments.meFragment;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayoutMediator;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.main.fragments.meFragment.adapter.MePagerAdapter;
import ir.nobahar.falp.databinding.FragmentMainMeBinding;

public class MainMeFragment extends Fragment implements MainMeView {

    public MainMeFragment() {
        // Required empty public constructor
    }

    private FragmentMainMeBinding binding;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_me, container, false);

        setupTabLayout();

        MainMePresenter presenter = new MainMePresenter(getActivity(), this);
        presenter.setupUserInformation();

        return binding.getRoot();
    }

    private void setupTabLayout() {
        MePagerAdapter pagerAdapter = new MePagerAdapter(Objects.requireNonNull(getActivity()));
        binding.viewPager.setAdapter(pagerAdapter);

        new TabLayoutMediator(binding.tabLayout, binding.viewPager, (tab, position) -> {
            switch (position) {
                case 0:
                    tab.setText(getActivity().getResources().getString(R.string.tabText_saves));
                    tab.setIcon(R.drawable.ic_bookmark);
                    break;

                case 1:
                    tab.setText(getActivity().getResources().getString(R.string.tabText_comments));
                    tab.setIcon(R.drawable.ic_comment);
                    break;

                case 2:
                    tab.setText(getActivity().getResources().getString(R.string.tabText_images));
                    tab.setIcon(R.drawable.ic_camera);
                    break;
            }
        }).attach();
    }

    @Override
    public void showUserInformation(String fullName, String gallery, String comments) {
        binding.usernameTxv.setText(fullName);
        binding.commentsTxv.setText(comments);
        binding.galleriesTxv.setText(gallery);
    }
}