package ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation.adapter.BusinessHours;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.RecyclerHoursBusinessInfoBinding;
import ir.nobahar.falp.model.business.BusinessWorkHours;

public class RecyclerBusinessHoursAdapter extends RecyclerView.Adapter<RecyclerBusinessHourViewHolder> {

    private final Context context;
    private final List<BusinessWorkHours> workHours;

    private String dayName = "";

    public RecyclerBusinessHoursAdapter(Context context, List<BusinessWorkHours> workHours) {
        this.context = context;
        this.workHours = workHours;
    }

    @NonNull
    @Override
    public RecyclerBusinessHourViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        RecyclerHoursBusinessInfoBinding bind = DataBindingUtil.inflate(inflater, R.layout.recycler_hours_business_info, parent, false);
        return new RecyclerBusinessHourViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerBusinessHourViewHolder holder, int position) {
        BusinessWorkHours hours = this.workHours.get(position);

        if (hours.getIsClosed().equals("1")) {
            holder.bind.timeTxv.setText(context.getResources().getString(R.string.it_is_closed));
            holder.bind.timeTxv.setTextColor(context.getResources().getColor(R.color.red));

        } else {
            holder.bind.timeTxv.setText(String.format("%s-%s", hours.getOpeningTime(), hours.getCloseTime()));
            holder.bind.timeTxv.setTextColor(context.getResources().getColor(R.color.black));
        }

        if (dayName.equals("")) dayName = hours.getDayName();
        else if (dayName.equals(hours.getDayName())) dayName = "";
        else dayName = hours.getDayName();
        
        switch (dayName) {
            case "0":
                holder.bind.dayNameTxv.setText(context.getResources().getString(R.string.saturday));
                break;

            case "1":
                holder.bind.dayNameTxv.setText(context.getResources().getString(R.string.sunday));
                break;

            case "2":
                holder.bind.dayNameTxv.setText(context.getResources().getString(R.string.monday));
                break;

            case "3":
                holder.bind.dayNameTxv.setText(context.getResources().getString(R.string.tuesday));
                break;

            case "4":
                holder.bind.dayNameTxv.setText(context.getResources().getString(R.string.wednesday));
                break;

            case "5":
                holder.bind.dayNameTxv.setText(context.getResources().getString(R.string.thursday));
                break;

            case "6":
                holder.bind.dayNameTxv.setText(context.getResources().getString(R.string.friday));
                break;

            default: holder.bind.dayNameTxv.setText("");
        }
    }

    @Override
    public int getItemCount() {
        if (workHours.isEmpty()) return 0;
        else return workHours.size();
    }

    public void update(List<BusinessWorkHours> workHours) {
        this.workHours.addAll(workHours);
        notifyDataSetChanged();
    }
}
