package ir.nobahar.falp.view.activities.filters;

import java.util.List;

import ir.nobahar.falp.model.amenities.Amenities;
import ir.nobahar.falp.model.amenities.TopAmenities;

public interface FiltersView {

    void showMessage(String message);

    void recommendedFilters(List<TopAmenities> topAmenities);

    void amenitiesFilters(List<Amenities> amenities);

    void setAttrsId(String id);
}
