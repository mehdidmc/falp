package ir.nobahar.falp.view.activities.businessDetail.fragments.businessImages.adapter.businessImages;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.BusinessImagesPicsItemLayoutBinding;
import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryImages;

public class BusinessGalleryImagesAdapter extends RecyclerView.Adapter<BusinessGalleryImagesViewHolder> {

    private final Context context;
    private final List<GalleryImages> images;

    public BusinessGalleryImagesAdapter(Context context, List<GalleryImages> images) {
        this.context = context;
        this.images = images;
    }

    @NonNull
    @Override
    public BusinessGalleryImagesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        BusinessImagesPicsItemLayoutBinding bind = DataBindingUtil.inflate(inflater, R.layout.business_images_pics_item_layout, parent, false);
        return new BusinessGalleryImagesViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull BusinessGalleryImagesViewHolder holder, int position) {
        GalleryImages image = this.images.get(position);
        Picasso.get().load(image.getImageUrl()).into(holder.bind.businessImagesImv);
    }

    @Override
    public int getItemCount() {
        if (images.isEmpty()) return 0;
        return images.size();
    }

    public void update(List<GalleryImages> images) {
        this.images.addAll(images);
        notifyDataSetChanged();
    }
}
