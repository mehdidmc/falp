package ir.nobahar.falp.view.activities.main.fragments.moreFragment;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.FragmentMainMoreBinding;

public class MainMoreFragment extends Fragment {

    public MainMoreFragment() {
        // Required empty public constructor
    }

    private FragmentMainMoreBinding binding;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_more, container, false);
        MainMoreClickHandler clickHandler = new MainMoreClickHandler(getContext());
        binding.setClickHandler(clickHandler);
        return binding.getRoot();
    }
}