package ir.nobahar.falp.view.activities.login.fragments.loginSecurityCode;

public interface LoginSecurityCodeView {

    void setResendBtnEnabled(boolean enabled);

    void setCounterDownTxv(String time);

    String getCodeEntry();

    String getPhone();

    void showMessage(String message);

    void setLoginBtnEnabled(boolean enabled);
}
