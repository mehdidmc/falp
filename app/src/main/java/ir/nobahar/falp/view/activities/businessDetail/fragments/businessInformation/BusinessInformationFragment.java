package ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation.adapter.BusinessHours.RecyclerBusinessHoursAdapter;
import ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation.adapter.businessAmenities.RecyclerBusinessAmenitiesAdapter;
import ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation.adapter.businessInfo.RecyclerBusinessInfoAdapter;
import ir.nobahar.falp.databinding.FragmentBusinessInformationBinding;
import ir.nobahar.falp.model.business.Business;
import ir.nobahar.falp.model.business.BusinessCategory;

public class BusinessInformationFragment extends Fragment implements BusinessInfoView {

    private Business business;

    public BusinessInformationFragment() {
        // Required empty public constructor
    }

    public BusinessInformationFragment(Business business) {
        this.business = business;
    }

    private FragmentBusinessInformationBinding bind;

    private RecyclerBusinessHoursAdapter hoursAdapter;
    private RecyclerBusinessAmenitiesAdapter amenitiesAdapter;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bind = DataBindingUtil.inflate(inflater, R.layout.fragment_business_information, container, false);

        BusinessInfoClickHandler clickHandler = new BusinessInfoClickHandler(getActivity(), bind);
        bind.setClickHandler(clickHandler);

        BusinessInfoPresenter presenter = new BusinessInfoPresenter(getActivity(), this);
        presenter.getBusinessInfo(business.getUrl(), "", "");

        setupBusinessInfoRecycler();
        setupBusinessHoursRecycler();
        setupBusinessAmenities();

        return bind.getRoot();
    }

    private void setupBusinessInfoRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerBusinessInfoAdapter adapter = new RecyclerBusinessInfoAdapter(Objects.requireNonNull(getContext()));

        bind.infoRecyclerView.setHasFixedSize(true);
        bind.infoRecyclerView.setLayoutManager(layoutManager);
        bind.infoRecyclerView.setAdapter(adapter);
    }

    private void setupBusinessAmenities() {
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        amenitiesAdapter = new RecyclerBusinessAmenitiesAdapter(getContext(), new ArrayList<>());

        bind.amenitiesRecyclerView.setHasFixedSize(true);
        bind.amenitiesRecyclerView.setNestedScrollingEnabled(false);
        bind.amenitiesRecyclerView.setLayoutManager(layoutManager);
        bind.amenitiesRecyclerView.setAdapter(amenitiesAdapter);
    }

    private void setupBusinessHoursRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        hoursAdapter = new RecyclerBusinessHoursAdapter(getContext(), new ArrayList<>());

        bind.hoursRecyclerView.setHasFixedSize(true);
        bind.hoursRecyclerView.setLayoutManager(layoutManager);
        bind.hoursRecyclerView.setAdapter(hoursAdapter);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setInfo(Business business) {
        if (business.getWorkHours().isEmpty() || business.getWorkHours() == null)
            bind.workHoursLayout.setVisibility(View.GONE);

        else {
            bind.workHoursLayout.setVisibility(View.VISIBLE);
            hoursAdapter.update(business.getWorkHours());
        }

        if (business.getAmenities().isEmpty() || business.getAmenities() == null)
            bind.businessAmenitiesLayout.setVisibility(View.GONE);

        else {
            bind.businessAmenitiesLayout.setVisibility(View.VISIBLE);
            amenitiesAdapter.update(business.getAmenities());
        }

        if (business.getCityId().equals("333"))
            bind.cityAddressTxv.setText(getString(R.string.city_id_1));

        String categoryTitle = "";
        for (BusinessCategory c : business.getCategories()) {
            categoryTitle = c.getTitle();
            categoryTitle += ", ";
        }
        bind.categoryDetailTxv.setText(categoryTitle);

        bind.detailAddressTxv.setText(String.format("%s %s", business.getAddress1(), business.getAddress2()));
        bind.timeTxv.setText(business.getOpenDetail());
    }
}