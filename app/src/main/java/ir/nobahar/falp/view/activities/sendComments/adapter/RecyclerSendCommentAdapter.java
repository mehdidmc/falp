package ir.nobahar.falp.view.activities.sendComments.adapter;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.SendCommentImagesItemLayoutBinding;

public class RecyclerSendCommentAdapter extends RecyclerView.Adapter<RecyclerSendCommentViewHolder> {

    private final List<Uri> images;

    public RecyclerSendCommentAdapter(List<Uri> images) {
        this.images = images;
    }

    @NonNull
    @Override
    public RecyclerSendCommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        SendCommentImagesItemLayoutBinding bind = DataBindingUtil.inflate(inflater, R.layout.send_comment_images_item_layout, parent, false);
        return new RecyclerSendCommentViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerSendCommentViewHolder holder, int position) {
        holder.bind.sendImageImv.setImageURI(images.get(position));

        holder.bind.removeImageImg.setOnClickListener(view -> {
            images.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, images.size());
        });
    }

    @Override
    public int getItemCount() {
        if (images.isEmpty()) return 0;
        return images.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void update(Uri uri) {
        this.images.add(uri);
        notifyDataSetChanged();
    }

    public List<Uri> getAllImages() {
        return this.images;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void removeAll() {
        this.images.clear();
        notifyDataSetChanged();
    }
}
