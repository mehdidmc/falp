package ir.nobahar.falp.view.activities.sendComments;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.ActivitySendCommentsBinding;
import ir.nobahar.falp.view.activities.sendComments.adapter.RecyclerSendCommentAdapter;
import ir.nobahar.falp.view.appDialog.AppDialog;
import ir.nobahar.falp.view.dialogProgress.DialogProgress;

public class SendCommentsActivity extends AppCompatActivity implements SendCommentClickHandler, SendCommentView {

    private ActivitySendCommentsBinding bind;

    private RecyclerSendCommentAdapter imageAdapter;
    private SendCommentPresenter presenter;
    private DialogProgress progress;

    private String businessId = "";
    private String businessUrl = "";
    private int rateScore = 0;
    private int imageCounts = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_comments);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_send_comments);
        bind.setClickHandler(this);

        presenter = new SendCommentPresenter(this, this);

        getExtras();
        setupRecyclerView();
        setupProgressDialog();
        setStarRate();
    }

    //Inner Methods Start {

    private void setupProgressDialog() {
        progress = DialogProgress.get();
        progress.create(this);
    }

    private void setupRecyclerView() {
        imageAdapter = new RecyclerSendCommentAdapter(new ArrayList<>());
        GridLayoutManager layoutManager = new GridLayoutManager(this, 4);

        bind.sendImageRecyclerView.setHasFixedSize(true);
        bind.sendImageRecyclerView.setLayoutManager(layoutManager);
        bind.sendImageRecyclerView.setAdapter(imageAdapter);
    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            businessId = bundle.getString("business_id");
            businessUrl = bundle.getString("business_url");
            rateScore = bundle.getInt("comment_rate");
            bind.setBusinessName(bundle.getString("business_name"));
        }
    }

    private void setStarRate() {
        if (rateScore == 1) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));

        } else if (rateScore == 2) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star2, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));

        } else if (rateScore == 3) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star2, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star3, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));

        } else if (rateScore == 4) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star2, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star3, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star4, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));

        } else if (rateScore == 5) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star2, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star3, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star4, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star5, null));
        }
    }

    //Click Handler Start {

    @Override
    public void onClickRateStar(View view) {
        if (view.getId() == R.id.firstStar_imv) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));

            rateScore = 1;

        } else if (view.getId() == R.id.secondStar_imv) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star2, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));

            rateScore = 2;

        } else if (view.getId() == R.id.thirdStar_imv) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star2, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star3, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));

            rateScore = 3;

        } else if (view.getId() == R.id.fourthStar_imv) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star2, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star3, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star4, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star0, null));

            rateScore = 4;

        } else {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star2, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star3, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star4, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.big_circle_star5, null));

            rateScore = 5;
        }
    }

    @Override
    public void onClickPickImage(View view) {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    @Override
    public void onClickBack(View view) {
        finish();
    }

    @Override
    public void onClickSendComment(View view) {
        String comment = bind.commentEdt.getText().toString();
        imageCounts = imageAdapter.getAllImages().size();

        if (comment.equals("")) {
            showMessage(getString(R.string.comment_not_empty));
            return;
        }

        progress.show();
        presenter.sendComment(comment, String.valueOf(rateScore), businessUrl);
    }

    //Click Handler End }

    //Override Methods Start {


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                if (result != null) imageAdapter.update(result.getUri());

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Objects.requireNonNull(result).getError().printStackTrace();
                showMessage(getString(R.string.image_cropper_error));
            }
        }
    }

    //Override Methods End }

    //View Methods start {

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void uploadImages(String commentId) {
        List<Uri> images = imageAdapter.getAllImages();

        if (imageCounts > 0) {
            presenter.sendImages(images.get(imageCounts - 1), businessId, commentId);

        } else {
            progress.dismiss();

            AppDialog appDialog = AppDialog.get();
            appDialog.create(this,
                    getString(R.string.send_comment_info),
                    getString(R.string.send_comment_info_content));

            appDialog.getIcon().setImageDrawable(ContextCompat.getDrawable(this, R.drawable.success_tick));
            appDialog.getNegativeButton().setVisibility(View.GONE);
            appDialog.getPositiveButton().setOnClickListener(view -> {
                appDialog.dismiss();
                imageAdapter.removeAll();
            });
            appDialog.show();
        }

        --imageCounts;
    }

    //View Methods End }
}