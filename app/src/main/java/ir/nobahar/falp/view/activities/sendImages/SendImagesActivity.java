package ir.nobahar.falp.view.activities.sendImages;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.ActivitySendImagesBinding;
import ir.nobahar.falp.view.activities.sendImages.adapter.RecyclerSendImageAdapter;
import ir.nobahar.falp.view.appDialog.AppDialog;
import ir.nobahar.falp.view.dialogProgress.DialogProgress;

public class SendImagesActivity extends AppCompatActivity implements SendImageClickHandler, SendImageView {

    private ActivitySendImagesBinding bind;

    private RecyclerSendImageAdapter imageAdapter;
    private SendImagePresenter presenter;

    private String businessId = "";
    private int imageCounter = 0;

    private DialogProgress progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_images);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_send_images);
        bind.setClickHandler(this);

        presenter = new SendImagePresenter(this, this);

        getExtras();
        setupRecyclerView();
        setupProgressDialog();
    }

    //Inner class methods Start {

    private void setupRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(this,3);
        imageAdapter = new RecyclerSendImageAdapter(new ArrayList<>());

        bind.imagesRecyclerView.setHasFixedSize(true);
        bind.imagesRecyclerView.setLayoutManager(layoutManager);
        bind.imagesRecyclerView.setAdapter(imageAdapter);
    }

    private void setupProgressDialog() {
        progress = DialogProgress.get();
        progress.create(this);
    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            businessId = bundle.getString("business_id");
            bind.setBusinessName(bundle.getString("business_name"));
        }
    }

    //Inner class methods Ends }

    //ClickHandlers Start {

    @Override
    public void onClickSendImage(View view) {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    @Override
    public void onClickUploadImage(View view) {
        imageCounter = imageAdapter.getAllImages().size();
        progress.show();
        uploadImages();
    }

    @Override
    public void onClickBack(View view) {
        finish();
    }

    //ClickHandlers End }

    //Override Methods Start {

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                if (result != null) imageAdapter.update(result.getUri());

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Objects.requireNonNull(result).getError().printStackTrace();
                showMessage(getString(R.string.image_cropper_error));
            }
        }
    }

    //Override Methods End }

    //View Methods Start {

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void uploadImages() {
        List<Uri> images = imageAdapter.getAllImages();
        List<String> captions = imageAdapter.getAllCaptions();

        if (imageCounter > 0)
            presenter.uploadImage(images.get(imageCounter - 1), captions.get(imageCounter - 1), businessId);

        else {
            progress.dismiss();

            AppDialog dialog = AppDialog.get();
            dialog.create(this,
                    getString(R.string.send_image_title),
                    getString(R.string.send_image_content_dialog));

            dialog.getIcon().setImageDrawable(ContextCompat.getDrawable(this, R.drawable.success_tick));
            dialog.getNegativeButton().setVisibility(View.GONE);
            dialog.getPositiveButton().setOnClickListener(view -> {
                imageAdapter.removeAll();
                dialog.dismiss();
            });
            dialog.show();
        }

        --imageCounter;
    }

    //View Methods End }
}