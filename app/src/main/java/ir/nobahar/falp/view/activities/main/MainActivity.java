package ir.nobahar.falp.view.activities.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.main.fragments.homeFragment.MainHomeFragment;
import ir.nobahar.falp.view.activities.main.fragments.meFragment.MainMeFragment;
import ir.nobahar.falp.view.activities.main.fragments.meNotRegisteredFragment.MainMeNotRegisteredFragment;
import ir.nobahar.falp.view.activities.main.fragments.moreFragment.MainMoreFragment;
import ir.nobahar.falp.databinding.ActivityMainBinding;
import ir.nobahar.falp.util.Assistant;
import ir.nobahar.falp.util.LoginChecking;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setDefaultView();

        binding.bottomNavigation.setOnNavigationItemSelectedListener(this);
    }

    private void setDefaultView() {
        Fragment fragment = new MainHomeFragment();
        Assistant.get().setFragmentToFrameNoAnimation(fragment, getSupportFragmentManager(), binding.navigationContentLayoutFrm.getId());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_bottomNav: {
                Assistant.get().setFragmentToFrameNoAnimation(new MainHomeFragment(), getSupportFragmentManager(), binding.navigationContentLayoutFrm.getId());
                return true;
            }

            case R.id.me_bottomNav: {
                boolean isLogin = LoginChecking.getInstance(this).getLoginMode();
                if (isLogin) Assistant.get().setFragmentToFrameNoAnimation(new MainMeFragment(), getSupportFragmentManager(), binding.navigationContentLayoutFrm.getId());
                else Assistant.get().setFragmentToFrameNoAnimation(new MainMeNotRegisteredFragment(), getSupportFragmentManager(), binding.navigationContentLayoutFrm.getId());
                return true;
            }

            case R.id.more_bottomNav: {
                Assistant.get().setFragmentToFrameNoAnimation(new MainMoreFragment(), getSupportFragmentManager(), binding.navigationContentLayoutFrm.getId());
                return true;
            }
        }

        return false;
    }
}