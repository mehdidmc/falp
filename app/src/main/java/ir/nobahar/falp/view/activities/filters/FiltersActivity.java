package ir.nobahar.falp.view.activities.filters;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.filters.adapters.amenitiesAdapter.RecyclerAmenitiesAdapter;
import ir.nobahar.falp.view.activities.filters.adapters.recommendedAdapter.RecyclerRecommendedAdapter;
import ir.nobahar.falp.view.activities.search.SearchActivity;
import ir.nobahar.falp.databinding.ActivityFiltersBinding;
import ir.nobahar.falp.model.amenities.Amenities;
import ir.nobahar.falp.model.amenities.TopAmenities;
import ir.nobahar.falp.model.businessSearch.SearchModel;

public class FiltersActivity extends AppCompatActivity implements FiltersClickHandler, FiltersView {

    private ActivityFiltersBinding bind;

    private RecyclerRecommendedAdapter recommendedAdapter;
    private RecyclerAmenitiesAdapter amenitiesAdapter;

    private SearchModel filter = null;

    private int numberOfFilters = 0;

    private final List<String> attrsId = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_filters);
        bind.setClickHandler(this);

        FiltersPresenter presenter = new FiltersPresenter(this, this);
        presenter.getAmenities("", "", "");

        setupFilters();
        getExtras();
        setupRecommendedRecyclerView();
        setupAmenitiesRecyclerView();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_left);
    }

    //Activity Methods Start {

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        String priceFilter = "";
        String sortFilter = "";
        String openNow = "";

        if (bundle != null) {
            priceFilter = bundle.getString("price_filter");
            sortFilter = bundle.getString("sort_filter");
            openNow = bundle.getString("open_now");
        }

        if (priceFilter.contains("0")) {
            bind.oneDollarCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.transparent_red_light, null));
            bind.oneDollarCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
            bind.oneDollarTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
        }

        if (priceFilter.contains("1")) {
            bind.twoDollarCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.transparent_red_light, null));
            bind.twoDollarCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
            bind.twoDollarTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
        }

        if (priceFilter.contains("2")) {
            bind.threeDollarCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.transparent_red_light, null));
            bind.threeDollarCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
            bind.threeDollarTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
        }

        if (priceFilter.contains("3")) {
            bind.fourDollarCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.transparent_red_light, null));
            bind.fourDollarCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
            bind.fourDollarTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
        }

        if (sortFilter.equals("0")) {
            bind.recommendedTxv.setTextColor(getResources().getColor(R.color.red));
            bind.recommendedCardView.setCardBackgroundColor(getResources().getColor(R.color.transparent_red_light));
            bind.recommendedCardView.setStrokeColor(getResources().getColor(R.color.red));

            bind.mostRankTxv.setTextColor(getResources().getColor(R.color.black));
            bind.mostRankCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            bind.mostRankCardView.setStrokeColor(getResources().getColor(R.color.black));

            bind.mostCommentTxv.setTextColor(getResources().getColor(R.color.black));
            bind.mostCommentCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            bind.mostCommentCardView.setStrokeColor(getResources().getColor(R.color.black));

        } else if (sortFilter.equals("3")) {
            bind.mostRankTxv.setTextColor(getResources().getColor(R.color.red));
            bind.mostRankCardView.setCardBackgroundColor(getResources().getColor(R.color.transparent_red_light));
            bind.mostRankCardView.setStrokeColor(getResources().getColor(R.color.red));

            bind.recommendedTxv.setTextColor(getResources().getColor(R.color.black));
            bind.recommendedCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            bind.recommendedCardView.setStrokeColor(getResources().getColor(R.color.black));

            bind.mostCommentTxv.setTextColor(getResources().getColor(R.color.black));
            bind.mostCommentCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            bind.mostCommentCardView.setStrokeColor(getResources().getColor(R.color.black));

        } else if (sortFilter.contains("4")) {
            bind.mostCommentTxv.setTextColor(getResources().getColor(R.color.red));
            bind.mostCommentCardView.setCardBackgroundColor(getResources().getColor(R.color.transparent_red_light));
            bind.mostCommentCardView.setStrokeColor(getResources().getColor(R.color.red));

            bind.recommendedTxv.setTextColor(getResources().getColor(R.color.black));
            bind.recommendedCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            bind.recommendedCardView.setStrokeColor(getResources().getColor(R.color.black));

            bind.mostRankTxv.setTextColor(getResources().getColor(R.color.black));
            bind.mostRankCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            bind.mostRankCardView.setStrokeColor(getResources().getColor(R.color.black));
        }

        filter.setPrice(priceFilter);
        filter.setSort(sortFilter);
        filter.setOpenNow(openNow);
    }

    private void setupFilters() {
        filter = new SearchModel();
        filter.setStart("0");
        filter.setCount("20");
        filter.setCityId("333");
        filter.setFilterCount(0, null);
        filter.setMaxLat(String.valueOf(SearchModel.DEFAULT_LAT + SearchModel.Y_ZOOM_12));
        filter.setMinLat(String.valueOf(SearchModel.DEFAULT_LAT - SearchModel.Y_ZOOM_12));
        filter.setMaxLng(String.valueOf(SearchModel.DEFAULT_LNG + SearchModel.X_ZOOM_12));
        filter.setMinLng(String.valueOf(SearchModel.DEFAULT_LNG - SearchModel.X_ZOOM_12));
    }

    private void setupRecommendedRecyclerView() {
        recommendedAdapter = new RecyclerRecommendedAdapter(this, new ArrayList<>());
        if (filter.getOpenNow().equals("1")) recommendedAdapter.setIsOpen(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        bind.recommendedRecyclerView.setHasFixedSize(true);
        bind.recommendedRecyclerView.setLayoutManager(layoutManager);
        bind.recommendedRecyclerView.setAdapter(recommendedAdapter);
    }

    private void setupAmenitiesRecyclerView() {
        amenitiesAdapter = new RecyclerAmenitiesAdapter(this, new ArrayList<>(), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        bind.amenitiesRecyclerView.setHasFixedSize(true);
        bind.amenitiesRecyclerView.setLayoutManager(layoutManager);
        bind.amenitiesRecyclerView.setAdapter(amenitiesAdapter);
    }

    // } Activity Methods End

    //Click Handler Methods begins {
    @Override
    public void onClickBack(View view) {
        finish();
    }

    @Override
    public void onClickSearch(View view) {
        Intent intent = new Intent(this, SearchActivity.class);

        List<String> recommendedItems = recommendedAdapter.getEnabledItemsId();
        StringBuilder recommendedId = new StringBuilder();

        List<String> amenitiesItems = attrsId;
        StringBuilder amenitiesId = new StringBuilder();

        String attrs = "";

        if (!recommendedItems.isEmpty()) {
            for (int i = 0; i < recommendedItems.size(); i++) {
                if (i == recommendedItems.size() - 1)
                    recommendedId.append(recommendedItems.get(i));
                else
                    recommendedId.append(recommendedItems.get(i)).append(",");
            }
        }

        if (!amenitiesItems.isEmpty()) {
            for (int i = 0; i < amenitiesItems.size(); i++) {
                if (i == amenitiesItems.size() - 1)
                    amenitiesId.append(amenitiesItems.get(i));
                else
                    amenitiesId.append(amenitiesItems.get(i)).append(",");
            }
        }

        if (recommendedId.length() != 0) {
            attrs = recommendedId.toString();
            numberOfFilters += recommendedItems.size();
        }

        if (amenitiesId.length() != 0) {
            if (attrs.equals(""))
                attrs += amenitiesId.toString();
            else
                attrs = "," + amenitiesId.toString();

            numberOfFilters += amenitiesItems.size();
        }

        if (recommendedAdapter.getIsOpen()) {
            ++numberOfFilters;
            filter.setOpenNow("1");
        }

        filter.setAttrs(attrs);
        filter.setFilterCount(numberOfFilters, null);

        intent.putExtra("backFilters", filter);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onCLickSortItem(View view) {
        String sort = "0";

        if (view.getId() == R.id.recommended_cardView) {
            sort = "0";

            bind.recommendedCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.transparent_red_light, null));
            bind.recommendedCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
            bind.recommendedTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.red, null));

            bind.mostRankCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
            bind.mostRankCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
            bind.mostRankTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));

            bind.mostCommentCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
            bind.mostCommentCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
            bind.mostCommentTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));

        } else if (view.getId() == R.id.mostRank_cardView) {
            sort = "3";

            bind.recommendedCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
            bind.recommendedCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
            bind.recommendedTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));

            bind.mostRankCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.transparent_red_light, null));
            bind.mostRankCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
            bind.mostRankTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.red, null));

            bind.mostCommentCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
            bind.mostCommentCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
            bind.mostCommentTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));

        } else if (view.getId() == R.id.mostComment_cardView) {
            sort = "4";

            bind.recommendedCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
            bind.recommendedCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
            bind.recommendedTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));

            bind.mostRankCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
            bind.mostRankCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
            bind.mostRankTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));

            bind.mostCommentCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.transparent_red_light, null));
            bind.mostCommentCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
            bind.mostCommentTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
        }

        filter.setSort(sort);
    }

    @Override
    public void onClickPriceItem(View view) {
        String price = filter.getPrice();
        String allPrice = "0,1,2,3";

        if (view.getId() == R.id.oneDollar_cardView) {
            if (bind.oneDollarCardView.getCardBackgroundColor().getDefaultColor() == ResourcesCompat.getColor(getResources(), R.color.white, null)) {
                bind.oneDollarCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.transparent_red_light, null));
                bind.oneDollarCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
                bind.oneDollarTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.red, null));

                if (price.isEmpty()) price = "0";
                else price += ",0";

                ++numberOfFilters;

            } else {
                bind.oneDollarCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
                bind.oneDollarCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
                bind.oneDollarTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));

                if (price.length() <= 1) price = allPrice;
                else price = price.replace(",0", "");

                if (numberOfFilters > 0) --numberOfFilters;
            }

        } else if (view.getId() == R.id.twoDollar_cardView) {
            if (bind.twoDollarCardView.getCardBackgroundColor().getDefaultColor() == ResourcesCompat.getColor(getResources(), R.color.white, null)) {
                bind.twoDollarCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.transparent_red_light, null));
                bind.twoDollarCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
                bind.twoDollarTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.red, null));

                if (price.isEmpty()) price = "1";
                else price += ",1";

                ++numberOfFilters;

            } else {
                bind.twoDollarCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
                bind.twoDollarCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
                bind.twoDollarTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));

                if (price.length() <= 1) price = allPrice;
                else price = price.replace(",1", "");

                if (numberOfFilters > 0) --numberOfFilters;
            }

        } else if (view.getId() == R.id.threeDollar_cardView) {
            if (bind.threeDollarCardView.getCardBackgroundColor().getDefaultColor() == ResourcesCompat.getColor(getResources(), R.color.white, null)) {
                bind.threeDollarCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.transparent_red_light, null));
                bind.threeDollarCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
                bind.threeDollarTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.red, null));

                if (price.isEmpty()) price = "2";
                else price += ",2";

                ++numberOfFilters;

            } else {
                bind.threeDollarCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
                bind.threeDollarCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
                bind.threeDollarTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));

                if (price.length() <= 1) price = allPrice;
                else price = price.replace(",2", "");

                if (numberOfFilters > 0) --numberOfFilters;
            }

        } else if (view.getId() == R.id.fourDollar_cardView) {
            if (bind.fourDollarCardView.getCardBackgroundColor().getDefaultColor() == ResourcesCompat.getColor(getResources(), R.color.white, null)) {
                bind.fourDollarCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.transparent_red_light, null));
                bind.fourDollarCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.red, null));
                bind.fourDollarTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.red, null));

                if (price.isEmpty()) price = "3";
                else price += ",3";

                ++numberOfFilters;

            } else {
                bind.fourDollarCardView.setCardBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.white, null));
                bind.fourDollarCardView.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
                bind.fourDollarTxv.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));

                if (price.length() <= 1) price = allPrice;
                else price = price.replace(",3", "");

                if (numberOfFilters > 0) --numberOfFilters;
            }
        }

        filter.setPrice(price);
        filter.setFilterCount(numberOfFilters, null);
    }
    //Click Handler Methods End {

    //Views implements Start {
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void recommendedFilters(List<TopAmenities> topAmenities) {
        recommendedAdapter.update(topAmenities);
    }

    @Override
    public void amenitiesFilters(List<Amenities> amenities) {
        amenitiesAdapter.update(amenities);
    }

    @Override
    public void setAttrsId(String id) {
        attrsId.add(id);
        Toast.makeText(this, "size > " + attrsId.size(), Toast.LENGTH_SHORT).show();
    }

    // } View implements End
}