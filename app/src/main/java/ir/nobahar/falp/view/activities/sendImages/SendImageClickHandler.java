package ir.nobahar.falp.view.activities.sendImages;

import android.view.View;

public interface SendImageClickHandler {

    void onClickSendImage(View view);

    void onClickUploadImage(View view);

    void onClickBack(View view);
}
