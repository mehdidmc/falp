package ir.nobahar.falp.view.activities.sendImages.adapter;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.RecyclerSendImagesItemsBinding;

public class RecyclerSendImageAdapter extends RecyclerView.Adapter<RecyclerSendImageViewHolder> {

    private final List<Uri> images;
    private final List<String> captions;

    public RecyclerSendImageAdapter(List<Uri> images) {
        this.images = images;
        this.captions = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerSendImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerSendImagesItemsBinding bind = DataBindingUtil.inflate(inflater, R.layout.recycler_send_images_items, parent, false);
        return new RecyclerSendImageViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerSendImageViewHolder holder, int position) {
        holder.bind.imageContent.setImageURI(images.get(position));

        final String[] caption = {""};
        int pos = position;

        captions.add(position, holder.bind.imageCaptionEdt.getText().toString());

        holder.bind.imageCaptionEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                caption[0] = holder.bind.imageCaptionEdt.getText().toString();
                captions.add(pos, caption[0]);
            }
        });

        holder.bind.removeImageImg.setOnClickListener(view -> {
            images.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, images.size());
        });
    }

    @Override
    public int getItemCount() {
        if (images.isEmpty()) return 0;
        return images.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void update(Uri uri) {
        this.images.add(uri);
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void removeAll() {
        this.images.clear();
        notifyDataSetChanged();
    }

    public List<Uri> getAllImages() {
        return this.images;
    }

    public List<String> getAllCaptions() {
        return this.captions;
    }
}
