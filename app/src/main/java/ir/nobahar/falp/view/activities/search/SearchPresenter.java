package ir.nobahar.falp.view.activities.search;

import android.content.Context;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.model.categorySearch.CategorySearchItems;
import ir.nobahar.falp.model.categorySearch.CategorySearchResponse;
import ir.nobahar.falp.model.businessSearch.BusinessSearchResponse;
import ir.nobahar.falp.model.businessSearch.SearchModel;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.SearchServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchPresenter {

    private final Context context;
    private final SearchView view;

    public SearchPresenter(Context context, SearchView view) {
        this.context = context;
        this.view = view;
    }

    public void searchByLocation(boolean isUpdate) {
        view.setRefreshing(true);
        SearchModel search = view.getSearchFilters();

        SearchServices searchServices = RetrofitInstance.getInstance().create(SearchServices.class);
        Call<BusinessSearchResponse> call = searchServices.searchByLocation(search.getStart(), search.getCount(), search.getMinLat(), search.getMaxLat(),
                search.getMinLng(), search.getMaxLng(), search.getSort(), search.getPrice(), search.getCtgs(), search.getOpenNow(),
                search.getAttrs(), search.getUserId(), search.getFindDesc(), search.getCityId());

        call.enqueue(new Callback<BusinessSearchResponse>() {
            @Override
            public void onResponse(@NotNull Call<BusinessSearchResponse> call, @NotNull Response<BusinessSearchResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (isUpdate) view.updateSearchResult(response.body().getBusinesses());
                    else view.changeSearchResult(response.body().getBusinesses());
                }
                else view.showMessage(context.getString(R.string.retrofit_response_error));
                view.setRefreshing(false);
            }

            @Override
            public void onFailure(@NotNull Call<BusinessSearchResponse> call, @NotNull Throwable t) {
                view.showMessage(context.getString(R.string.retrofit_onFailure));
                t.printStackTrace();
                view.setRefreshing(false);
            }
        });
    }

    public void searchByCategory(String searchTitle) {
        String cityId = context.getResources().getString(R.string.tehran_city_code);

        SearchServices services = RetrofitInstance.getInstance().create(SearchServices.class);
        Call<CategorySearchResponse> call = services.searchByCategory(searchTitle, cityId);
        call.enqueue(new Callback<CategorySearchResponse>() {
            @Override
            public void onResponse(@NotNull Call<CategorySearchResponse> call, @NotNull Response<CategorySearchResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getItems() != null) {
                        List<String> categorySearch = new ArrayList<>();

                        for (CategorySearchItems items : response.body().getItems())
                            categorySearch.add(items.getTitle());

                        view.setAutoCompleteItems(categorySearch);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<CategorySearchResponse> call, @NotNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void updateRecyclerView(int lastStartItem) {
        SearchModel filter = view.getSearchFilters();
        int currentStartItem = lastStartItem + 20;
        filter.setStart(String.valueOf(currentStartItem));
        view.setSearchFilters(filter);
        searchByLocation(true);
    }
}
