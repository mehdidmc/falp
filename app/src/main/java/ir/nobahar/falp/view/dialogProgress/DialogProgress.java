package ir.nobahar.falp.view.dialogProgress;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;

import androidx.databinding.DataBindingUtil;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.ProgressDialogLayoutBinding;

public class DialogProgress {

    private static DialogProgress instance = null;

    private Dialog dialog;

    private DialogProgress() {}

    public static synchronized DialogProgress get() {
        if (instance == null) instance = new DialogProgress();
        return instance;
    }

    public void create(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ProgressDialogLayoutBinding bind = DataBindingUtil.inflate(inflater, R.layout.progress_dialog_layout, null, false);
        dialog = new Dialog(context);

        dialog.setContentView(bind.getRoot());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }
}
