package ir.nobahar.falp.view.activities.login.fragments.loginSecurityCode;

import android.view.View;

import androidx.fragment.app.FragmentActivity;

public class LoginSecurityCodeClickHandler {

    private final LoginSecurityCodePresenter presenter;
    private final FragmentActivity activity;

    public LoginSecurityCodeClickHandler(FragmentActivity activity, LoginSecurityCodePresenter presenter) {
        this.activity = activity;
        this.presenter = presenter;
    }

    public void onCLickBack(View view) {
        activity.getSupportFragmentManager().popBackStack();
    }

    public void onCLickLoginBtn(View view) {
        presenter.loginToApp();
    }

    public void onCLickResendCodeBtn(View view) {
        presenter.resendSecurityCode();
    }
}
