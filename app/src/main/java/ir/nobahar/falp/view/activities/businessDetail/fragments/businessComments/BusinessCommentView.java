package ir.nobahar.falp.view.activities.businessDetail.fragments.businessComments;

import java.util.List;

import ir.nobahar.falp.model.comments.Comments;

public interface BusinessCommentView {

    void showMessage(String message);

    void updateComments(List<Comments> comments);
}
