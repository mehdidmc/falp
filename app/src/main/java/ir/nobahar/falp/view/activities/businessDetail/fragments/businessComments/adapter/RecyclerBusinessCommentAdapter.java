package ir.nobahar.falp.view.activities.businessDetail.fragments.businessComments.adapter;

import android.content.Context;
import android.transition.AutoTransition;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.BusinessCommentsItemsBinding;
import ir.nobahar.falp.model.comments.CommentImages;
import ir.nobahar.falp.model.comments.Comments;
import ir.nobahar.falp.model.user.User;

public class RecyclerBusinessCommentAdapter extends RecyclerView.Adapter<RecyclerBusinessCommentViewHolder> {

    private final Context context;
    private final List<Comments> comments;

    public RecyclerBusinessCommentAdapter(Context context, List<Comments> comments) {
        this.context = context;
        this.comments = comments;
    }

    @NonNull
    @Override
    public RecyclerBusinessCommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        BusinessCommentsItemsBinding bind = DataBindingUtil.inflate(inflater, R.layout.business_comments_items, parent, false);
        return new RecyclerBusinessCommentViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerBusinessCommentViewHolder holder, int position) {
        Comments comment = comments.get(position);
        User user = comment.getUser().get(0);
        List<CommentImages> images = comment.getCommentImages();

        Picasso.get().load(user.getAvatar()).into(holder.bind.userImageImv);

        holder.bind.usernameTxv.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
        holder.bind.userCommentsTxv.setText(user.getAllComments());
        holder.bind.userImagesTxv.setText(user.getAllImages());
        holder.bind.commentContentTxv.setText(comment.getComment());
        holder.bind.coolCounterTxv.setText(comment.getFeedback1());
        holder.bind.funCounterTxv.setText(comment.getFeedback2());
        holder.bind.usefulCounterTxv.setText(comment.getFeedback3());

        if (images != null) {
            if (images.size() == 3) {
                Picasso.get().load(images.get(0).getFileAddress()).into(holder.bind.sendImage1Imv);
                Picasso.get().load(images.get(1).getFileAddress()).into(holder.bind.sendImage2Imv);
                Picasso.get().load(images.get(2).getFileAddress()).into(holder.bind.sendImage3Imv);
                holder.bind.sendImage4Imv.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));

                if (images.size() > 3) holder.bind.moreImageTxv.setVisibility(View.VISIBLE);

            } else if (images.size() == 2) {
                Picasso.get().load(images.get(0).getFileAddress()).into(holder.bind.sendImage1Imv);
                Picasso.get().load(images.get(1).getFileAddress()).into(holder.bind.sendImage2Imv);
                holder.bind.sendImage3Imv.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
                holder.bind.sendImage4Imv.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));

            } else if (images.size() == 1){
                Picasso.get().load(images.get(0).getFileAddress()).into(holder.bind.sendImage1Imv);
                holder.bind.sendImage2Imv.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
                holder.bind.sendImage3Imv.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
                holder.bind.sendImage4Imv.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
            }
        }

        holder.bind.readMoreTxv.setOnClickListener(view -> {
            Transition transition = new AutoTransition();

            if (holder.bind.commentContentTxv.getLineCount() > 3) {
                if (holder.bind.readMoreTxv.getText().equals(context.getString(R.string.read_more))) {
                    TransitionManager.beginDelayedTransition(holder.bind.commentContentLayout, transition);
                    TransitionManager.beginDelayedTransition(holder.bind.sendImagesLayout, transition);
                    holder.bind.commentContentLayout.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                    holder.bind.commentContentLayout.requestLayout();
                    holder.bind.readMoreTxv.setText(context.getString(R.string.close_comment));


                } else {
                    float heightSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, context.getResources().getDisplayMetrics());
                    TransitionManager.beginDelayedTransition(holder.bind.commentContentLayout, transition);
                    TransitionManager.beginDelayedTransition(holder.bind.sendImagesLayout, transition);
                    holder.bind.commentContentLayout.getLayoutParams().height = (int) heightSize;
                    holder.bind.commentContentLayout.requestLayout();
                    holder.bind.readMoreTxv.setText(context.getString(R.string.read_more));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (comments.isEmpty()) return 0;
        return comments.size();
    }

    public void update(List<Comments> comments) {
        this.comments.addAll(comments);
        notifyDataSetChanged();
    }
}
