package ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation.adapter.businessAmenities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.BusinessInfoAmenitiesItemsBinding;
import ir.nobahar.falp.model.businessDetail.BusinessInfo.BusinessAmenities;
import ir.nobahar.falp.util.Assistant;

public class RecyclerBusinessAmenitiesAdapter extends RecyclerView.Adapter<RecyclerBusinessAmenitiesViewHolder> {

    private final Context context;
    private final List<BusinessAmenities> amenities;

    public RecyclerBusinessAmenitiesAdapter(Context context, List<BusinessAmenities> amenities) {
        this.context = context;
        this.amenities = amenities;
    }

    @NonNull
    @Override
    public RecyclerBusinessAmenitiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        BusinessInfoAmenitiesItemsBinding bind = DataBindingUtil.inflate(inflater, R.layout.business_info_amenities_items, parent, false);
        return new RecyclerBusinessAmenitiesViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerBusinessAmenitiesViewHolder holder, int position) {
        BusinessAmenities amenity = amenities.get(position);
        holder.bind.titleTxv.setText(amenity.getTitle()); 
        holder.bind.fontAwesome.setText(Assistant.get().setFontAwesome(amenity.getUnicode()));
    }

    @Override
    public int getItemCount() {
        if (amenities.isEmpty()) return 0;
        else return amenities.size();
    }

    public void update(List<BusinessAmenities> amenities) {
        this.amenities.addAll(amenities);
        notifyDataSetChanged();
    }
}
