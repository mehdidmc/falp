package ir.nobahar.falp.view.activities.login.fragments.loginUserProfile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.FragmentLoginUserProfileBinding;
import ir.nobahar.falp.model.user.User;

import static android.app.Activity.RESULT_OK;

public class LoginUserProfileFragment extends Fragment implements LoginUserProfileView {

    private FragmentLoginUserProfileBinding binding;
    private LoginUserProfileClickHandler clickHandler;

    public LoginUserProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login_user_profile, container, false);

        LoginUserProfilePresenter presenter = new LoginUserProfilePresenter(getActivity(), this);
        clickHandler = new LoginUserProfileClickHandler(presenter, getActivity(), this);

        binding.setClickHandler(clickHandler);

        return binding.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                binding.userProfileImv.setImageURI(Objects.requireNonNull(result).getUri());
                clickHandler.setAvatar(result.getUri());

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Objects.requireNonNull(result).getError().printStackTrace();
                showMessage(Objects.requireNonNull(getContext()).getString(R.string.image_cropper_error));
            }
        }
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public User checkInputValidation() {
        String name = Objects.requireNonNull(binding.nameEdt).getText().toString();
        String family = Objects.requireNonNull(binding.familyEdt).getText().toString();

        if (name.isEmpty()) {
            showMessage(getString(R.string.name_empty_error));
            return null;

        } else if (family.isEmpty()) {
            showMessage(getString(R.string.family_empty_error));
            return null;

        } else {
            User user = new User();
            user.setFirstName(name);
            user.setLastName(family);
            user.setCountryId(Objects.requireNonNull(getContext()).getString(R.string.iran_country_code));
            user.setCityId(getContext().getString(R.string.tehran_city_code));
            user.setPhone(getPhoneNumberArgument());
            user.setEmail("");
            user.setPassword("");
            return user;
        }
    }

    @Override
    public boolean avatarImageValidation() {
        BitmapDrawable imageViewContentDrawable = (BitmapDrawable) binding.userProfileImv.getDrawable();
        Bitmap imageViewContentBitmap = imageViewContentDrawable.getBitmap();

        BitmapDrawable defaultContentDrawable = (BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.user_image, null);
        Bitmap defaultContentBitmap = Objects.requireNonNull(defaultContentDrawable).getBitmap();

        return !imageViewContentBitmap.sameAs(defaultContentBitmap);
    }

    private String getPhoneNumberArgument() {
        return Objects.requireNonNull(getArguments()).getString("phone_number");
    }
}