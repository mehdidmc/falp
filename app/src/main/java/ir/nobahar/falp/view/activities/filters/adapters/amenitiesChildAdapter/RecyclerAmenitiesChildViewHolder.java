package ir.nobahar.falp.view.activities.filters.adapters.amenitiesChildAdapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import ir.nobahar.falp.databinding.ChildAmenitiesContentItemBinding;

public class RecyclerAmenitiesChildViewHolder extends RecyclerView.ViewHolder {

    public ChildAmenitiesContentItemBinding bind;

    public RecyclerAmenitiesChildViewHolder(@NonNull @NotNull ChildAmenitiesContentItemBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
