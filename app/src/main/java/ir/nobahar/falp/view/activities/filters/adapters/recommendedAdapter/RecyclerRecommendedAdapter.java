package ir.nobahar.falp.view.activities.filters.adapters.recommendedAdapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.RecommendedItemsLayoutBinding;
import ir.nobahar.falp.model.amenities.TopAmenities;

public class RecyclerRecommendedAdapter extends RecyclerView.Adapter<RecyclerRecommendedViewHolder> {

    private final Activity activity;
    private final List<TopAmenities> amenities;

    private boolean isOpen = false;

    private final List<String> enabledItemsId;

    public RecyclerRecommendedAdapter(Activity activity, List<TopAmenities> amenities) {
        this.activity = activity;
        this.amenities = amenities;
        enabledItemsId = new ArrayList<>();
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerRecommendedViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        RecommendedItemsLayoutBinding bind = DataBindingUtil.inflate(inflater, R.layout.recommended_items_layout, parent, false);
        return new RecyclerRecommendedViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerRecommendedViewHolder holder, int position) {
        TopAmenities amenities = this.amenities.get(position);
        holder.bind.titleSwitch.setText(amenities.getTitle());

        if (position == 1) if (isOpen) holder.bind.titleSwitch.setChecked(true);

        holder.bind.titleSwitch.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            int amenityId = Integer.parseInt(amenities.getId());
            if (isChecked) {
                if (amenityId != 101 && amenityId != 201)
                    enabledItemsId.add(String.valueOf(amenityId));

                else if (amenityId == 201)
                    isOpen = true;
            }
        });
    }

    @Override
    public int getItemCount() {
        if (amenities.isEmpty()) return 0;
        return amenities.size();
    }

    public void update(List<TopAmenities> amenities) {
        this.amenities.addAll(amenities);
        notifyDataSetChanged();
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public boolean getIsOpen() {
        return this.isOpen;
    }

    public List<String> getEnabledItemsId() {
        return enabledItemsId;
    }
}
