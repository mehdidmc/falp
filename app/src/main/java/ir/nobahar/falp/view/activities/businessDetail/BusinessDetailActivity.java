package ir.nobahar.falp.view.activities.businessDetail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayoutMediator;
import com.squareup.picasso.Picasso;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.businessDetail.adapter.BusinessTabAdapter;
import ir.nobahar.falp.databinding.ActivityBusinessDetailBinding;
import ir.nobahar.falp.model.business.Business;
import ir.nobahar.falp.model.business.BusinessGallery;

public class BusinessDetailActivity extends AppCompatActivity {

    private ActivityBusinessDetailBinding bind;

    private Business business;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_detail);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_business_detail);

        getExtras();
        setupAppBarAndTabLayout();
    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        business = (Business) bundle.getSerializable("business");
    }

    private void setupAppBarAndTabLayout() {
        BusinessTabAdapter tabAdapter = new BusinessTabAdapter(this, this, business);
        bind.viewPager.setAdapter(tabAdapter);

        bind.collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.white));
        bind.collapsingToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.white));

        bind.toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        bind.toolbar.setNavigationOnClickListener(view-> finish());
        bind.toolbar.setTitle(business.getName());
        setSupportActionBar(bind.toolbar);

        new TabLayoutMediator(bind.tabLayout, bind.viewPager, (tab, position) -> {
            String[] tabTitle = getResources().getStringArray(R.array.business_tab_title);
            tab.setText(tabTitle[position]);
        }).attach();

        if (business.getGalleries() != null && !business.getGalleries().isEmpty()) {
            BusinessGallery gallery = business.getGalleries().get(0);
            Picasso.get().load(gallery.getImageUrl()).into(bind.appBarImageImv);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}