package ir.nobahar.falp.view.activities.businessDetail.fragments.businessComments;

import android.app.Activity;

import org.jetbrains.annotations.NotNull;

import ir.nobahar.falp.R;
import ir.nobahar.falp.model.comments.CommentResponse;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.BusinessDetailServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessCommentPresenter {

    private final Activity activity;
    private final BusinessCommentView view;

    public BusinessCommentPresenter(Activity activity, BusinessCommentView view) {
        this.activity = activity;
        this.view = view;
    }

    public void getComments(String url, String userId, String start, String count) {
        BusinessDetailServices services = RetrofitInstance.getInstance().create(BusinessDetailServices.class);
        Call<CommentResponse> call = services.getComments(url, userId, start, count);
        call.enqueue(new Callback<CommentResponse>() {
            @Override
            public void onResponse(@NotNull Call<CommentResponse> call, @NotNull Response<CommentResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    view.updateComments(response.body().getComments());
                }
            }

            @Override
            public void onFailure(@NotNull Call<CommentResponse> call, @NotNull Throwable t) {
                view.showMessage(activity.getResources().getString(R.string.retrofit_onFailure));
                t.printStackTrace();
            }
        });
    }
}
