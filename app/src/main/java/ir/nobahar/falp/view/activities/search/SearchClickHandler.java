package ir.nobahar.falp.view.activities.search;

import android.view.View;

public interface SearchClickHandler {

    void onCLickClearSearch(View view);

    void onCLickSearch(View view);

    void onClickFilters(View view);

    void onClickOpenNow(View view);

    void onClickExpandSort(View view);

    void onCLickExpandPrice(View view);

    void onClickPrice(View view);

    void onCLickSort(View view);

    void onClickRemoveFilters(View view);
}
