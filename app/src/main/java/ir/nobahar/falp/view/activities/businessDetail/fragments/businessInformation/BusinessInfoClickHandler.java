package ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation;

import android.app.Activity;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.FragmentBusinessInformationBinding;

public class BusinessInfoClickHandler {

    private final Activity activity;
    private final FragmentBusinessInformationBinding bind;

    public BusinessInfoClickHandler(Activity activity, FragmentBusinessInformationBinding bind) {
        this.activity = activity;
        this.bind = bind;
    }

    public void onCLickMoreAmenities(View view) {
        if (bind.moreAmenitiesTxv.getText().equals(activity.getString(R.string.more_amenities))) {
            TransitionManager.beginDelayedTransition(bind.amenitiesLayout, new AutoTransition());
            bind.amenitiesLayout.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
            bind.amenitiesLayout.requestLayout();
            bind.moreAmenitiesTxv.setText(activity.getString(R.string.close_amenities));

        } else {
            float heightSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 140, activity.getResources().getDisplayMetrics());
            TransitionManager.beginDelayedTransition(bind.amenitiesLayout, new AutoTransition());
            bind.amenitiesLayout.getLayoutParams().height = (int) heightSize;
            bind.amenitiesLayout.requestLayout();
            bind.moreAmenitiesTxv.setText(activity.getString(R.string.more_amenities));
        }
    }
}
