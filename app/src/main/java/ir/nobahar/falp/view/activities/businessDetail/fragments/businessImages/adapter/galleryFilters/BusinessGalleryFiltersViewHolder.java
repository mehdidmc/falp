package ir.nobahar.falp.view.activities.businessDetail.fragments.businessImages.adapter.galleryFilters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.nobahar.falp.databinding.BusinessImagesFiltersItemsLayoutBinding;

public class BusinessGalleryFiltersViewHolder extends RecyclerView.ViewHolder {

    public BusinessImagesFiltersItemsLayoutBinding bind;

    public BusinessGalleryFiltersViewHolder(@NonNull BusinessImagesFiltersItemsLayoutBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
