package ir.nobahar.falp.view.activities.main.fragments.homeFragment;

import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.FragmentMainHomeBinding;

public class MainHomeFragment extends Fragment implements MainHomeView {

    public MainHomeFragment() {
        // Required empty public constructor
    }

    private FragmentMainHomeBinding binding;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_home, container, false);

        MainHomePresenter presenter = new MainHomePresenter(this, getActivity());
        MainHomeClickHandler clickHandler = new MainHomeClickHandler(getActivity());

        binding.setClickHandler(clickHandler);

        presenter.getSliderImage();

        binding.appBar.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange())
                binding.toolbarSearch.setVisibility(View.VISIBLE);

            else binding.toolbarSearch.setVisibility(View.GONE);
        });

        return binding.getRoot();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setDefaultSliderImages() {
        binding.randomImageImg.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.default_slider_image, null));
    }

    @Override
    public void setSlideUrlImage(String url) {
        Picasso.get().load(url).into(binding.randomImageImg);
    }
}