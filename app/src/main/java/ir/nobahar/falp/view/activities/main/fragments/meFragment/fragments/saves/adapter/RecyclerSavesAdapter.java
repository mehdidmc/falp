package ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.saves.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.MeSavedItemsBinding;
import ir.nobahar.falp.model.business.Business;
import ir.nobahar.falp.model.business.BusinessCategory;
import ir.nobahar.falp.model.business.BusinessGallery;

public class RecyclerSavesAdapter extends RecyclerView.Adapter<RecyclerSavesViewHolder> {

    private final Context context;
    private final List<Business> businesses;

    public RecyclerSavesAdapter(Context context, List<Business> businesses) {
        this.context = context;
        this.businesses = businesses;
    }

    @NonNull
    @Override
    public RecyclerSavesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        MeSavedItemsBinding binding = DataBindingUtil.inflate(inflater, R.layout.me_saved_items, parent, false);
        return new RecyclerSavesViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerSavesViewHolder holder, int position) {
        Business business = businesses.get(position);
        List<BusinessCategory> categories = business.getCategories();
        List<BusinessGallery> galleries = business.getGalleries();

        holder.binding.setBusiness(business);

        StringBuilder category = new StringBuilder();
        for (BusinessCategory c : categories) {
            category.append(String.format("%s،", c.getTitle()));
        }

        holder.binding.setCategory(category.toString());

        validatingGalleryImages(galleries, holder);
        validatingBusinessPrice(business.getPrice(), holder);
        validatingBusinessScore(business.getCommentsScoreCounts(), holder);

        holder.binding.unBookmarkImg.setOnClickListener(v -> {/*click listener*/});
    }

    private void validatingGalleryImages(List<BusinessGallery> galleries, RecyclerSavesViewHolder holder) {
        if (!galleries.isEmpty()) {
            if (galleries.size() == 1) {
                Picasso.get().load(galleries.get(0).getImageUrl()).into(holder.binding.workImage1Img);
                holder.binding.workImage2Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
                holder.binding.workImage3Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
                holder.binding.workImage4Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
            }

            if (galleries.size() == 2) {
                Picasso.get().load(galleries.get(0).getImageUrl()).into(holder.binding.workImage1Img);
                Picasso.get().load(galleries.get(1).getImageUrl()).into(holder.binding.workImage2Img);
                holder.binding.workImage3Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
                holder.binding.workImage4Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
            }

            if (galleries.size() == 3) {
                Picasso.get().load(galleries.get(0).getImageUrl()).into(holder.binding.workImage1Img);
                Picasso.get().load(galleries.get(1).getImageUrl()).into(holder.binding.workImage2Img);
                Picasso.get().load(galleries.get(2).getImageUrl()).into(holder.binding.workImage3Img);
                holder.binding.workImage4Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
            }

            if (galleries.size() == 4) {
                Picasso.get().load(galleries.get(0).getImageUrl()).into(holder.binding.workImage1Img);
                Picasso.get().load(galleries.get(1).getImageUrl()).into(holder.binding.workImage2Img);
                Picasso.get().load(galleries.get(2).getImageUrl()).into(holder.binding.workImage3Img);
                Picasso.get().load(galleries.get(3).getImageUrl()).into(holder.binding.workImage4Img);
            }

        } else {
            holder.binding.workImage1Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
            holder.binding.workImage2Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
            holder.binding.workImage3Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
            holder.binding.workImage4Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
        }
    }

    private void validatingBusinessPrice(String price, RecyclerSavesViewHolder holder) {
        if (price.equals("0"))
            holder.binding.dollarTextView.setText("$");

        if (price.equals("1"))
            holder.binding.dollarTextView.setText("$$");

        if (price.equals("2"))
            holder.binding.dollarTextView.setText("$$$");

        if (price.equals("3"))
            holder.binding.dollarTextView.setText("$$$$");
    }

    private void validatingBusinessScore(String score, RecyclerSavesViewHolder holder) {
        if (score.equals("0"))
            holder.binding.starsImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.starts_0));

        if (score.equals("1"))
            holder.binding.starsImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.starts_1));

        if (score.equals("2"))
            holder.binding.starsImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.starts_2));

        if (score.equals("3"))
            holder.binding.starsImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.starts_3));

        if (score.equals("4"))
            holder.binding.starsImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.starts_4));

        if (score.equals("5"))
            holder.binding.starsImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.starts_5));
    }

    @Override
    public int getItemCount() {
        if (businesses.isEmpty()) return 0;
        else return businesses.size();
    }

    public void update(List<Business> businesses) {
        if (businesses != null) this.businesses.addAll(businesses);
        notifyDataSetChanged();
    }
}
