package ir.nobahar.falp.view.activities.businessDetail.fragments.businessImages;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.arasthel.spannedgridlayoutmanager.SpanSize;
import com.arasthel.spannedgridlayoutmanager.SpannedGridLayoutManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ir.nobahar.falp.R;
import ir.nobahar.falp.util.LoginChecking;
import ir.nobahar.falp.view.activities.businessDetail.fragments.businessImages.adapter.businessImages.BusinessGalleryImagesAdapter;
import ir.nobahar.falp.view.activities.businessDetail.fragments.businessImages.adapter.galleryFilters.BusinessGalleryFiltersAdapter;
import ir.nobahar.falp.databinding.FragmentBusinessImagesBinding;
import ir.nobahar.falp.model.business.Business;
import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryFilters;
import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryImages;
import ir.nobahar.falp.view.activities.login.LoginActivity;
import ir.nobahar.falp.view.activities.sendImages.SendImagesActivity;
import ir.nobahar.falp.view.appDialog.AppDialog;

public class BusinessImagesFragment extends Fragment implements BusinessImagesView, BusinessImagesClickHandler {

    private Business business;

    public BusinessImagesFragment() {
        // Required empty public constructor
    }

    public BusinessImagesFragment(Business business) {
        this.business = business;
    }

    private FragmentBusinessImagesBinding bind;
    private BusinessGalleryFiltersAdapter filtersAdapter;
    private BusinessGalleryImagesAdapter imagesAdapter;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bind = DataBindingUtil.inflate(inflater, R.layout.fragment_business_images, container, false);
        bind.setClickHandler(this);

        BusinessImagesPresenter presenter = new BusinessImagesPresenter(getActivity(), this);

        setupFiltersRecyclerView();
        setupImagesRecyclerView();

        presenter.getGalleryImagesFilter(business.getBizHashId(), "");
        presenter.getBusinessImages(business.getBizHashId(), "1", "0", "20");

        return bind.getRoot();
    }

    private void setupFiltersRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        filtersAdapter = new BusinessGalleryFiltersAdapter(getContext(), new ArrayList<>());

        bind.filtersRecyclerView.setHasFixedSize(true);
        bind.filtersRecyclerView.setLayoutManager(layoutManager);
        bind.filtersRecyclerView.setAdapter(filtersAdapter);
    }

    private void setupImagesRecyclerView() {
        SpannedGridLayoutManager layoutManager = new SpannedGridLayoutManager(SpannedGridLayoutManager.Orientation.VERTICAL, 3);
        layoutManager.setItemOrderIsStable(true);

        layoutManager.setSpanSizeLookup(new SpannedGridLayoutManager.SpanSizeLookup(position -> {
            if (position % 6 == 0 || position % 6 == 4) return new SpanSize(2, 2);
            else  return new SpanSize(1, 1);
        }));

        imagesAdapter = new BusinessGalleryImagesAdapter(getContext(), new ArrayList<>());

        bind.imagesRecyclerView.setHasFixedSize(true);
        bind.imagesRecyclerView.setLayoutManager(layoutManager);
        bind.imagesRecyclerView.setAdapter(imagesAdapter);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateFilterAdapter(List<GalleryFilters> filters) {
        if (filters != null && !filters.isEmpty()) {
            this.filtersAdapter.update(filters);
            bind.filtersTxv.setVisibility(View.VISIBLE);

        } else bind.filtersTxv.setVisibility(View.GONE);
    }

    @Override
    public void updateImagesAdapter(List<GalleryImages> images) {
        if (images != null) {
            this.imagesAdapter.update(images);
            bind.noImageTxv.setVisibility(View.GONE);
        }
        else {
            bind.noImageTxv.setVisibility(View.VISIBLE);
        }
    }

    // Click Handler Start {

    @Override
    public void onClickSendImage(View view) {
        if (LoginChecking.getInstance(getActivity()).getLoginMode()) {
            Intent intent = new Intent(getActivity(), SendImagesActivity.class);
            intent.putExtra("business_id", business.getBizHashId());
            intent.putExtra("business_name", business.getName());
            Objects.requireNonNull(getActivity()).startActivity(intent);

        } else {
            AppDialog dialog = AppDialog.get();
            dialog.create(
                    getActivity(),
                    Objects.requireNonNull(getActivity()).getString(R.string.no_login_title),
                    getActivity().getString(R.string.no_login_content));

            dialog.getPositiveButton().setText(getActivity().getString(R.string.positive_login));
            dialog.getPositiveButton().setOnClickListener(v -> {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                getActivity().startActivity(intent);
                dialog.dismiss();
            });

            dialog.show();
        }
    }

    //Click Handler End }
}