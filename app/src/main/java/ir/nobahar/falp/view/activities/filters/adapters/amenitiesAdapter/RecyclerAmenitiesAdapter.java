package ir.nobahar.falp.view.activities.filters.adapters.amenitiesAdapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.filters.FiltersView;
import ir.nobahar.falp.view.activities.filters.adapters.amenitiesChildAdapter.RecyclerAmenitiesChildAdapter;
import ir.nobahar.falp.databinding.FatherAmenitiesTitleItemsBinding;
import ir.nobahar.falp.model.amenities.Amenities;

public class RecyclerAmenitiesAdapter extends RecyclerView.Adapter<RecyclerAmenitiesViewHolder> {

    private final Activity activity;
    private final List<Amenities> amenities;
    private final FiltersView view;

    private RecyclerAmenitiesChildAdapter childAdapter;

    public RecyclerAmenitiesAdapter(Activity activity, List<Amenities> amenities, FiltersView view) {
        this.activity = activity;
        this.amenities = amenities;
        this.view = view;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerAmenitiesViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        FatherAmenitiesTitleItemsBinding bind = DataBindingUtil.inflate(inflater, R.layout.father_amenities_title_items, parent, false);
        return new RecyclerAmenitiesViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerAmenitiesViewHolder holder, int position) {
        Amenities amenity = this.amenities.get(position);
        holder.bind.amenityTitleTxv.setText(amenity.getAmenitiesTitle());

        setupChildRecyclerView(holder.bind);
        childAdapter.update(amenity.getChildren());
    }

    @Override
    public int getItemCount() {
        if (amenities.isEmpty()) return 0;
        return amenities.size();
    }

    private void setupChildRecyclerView(FatherAmenitiesTitleItemsBinding bind) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        childAdapter = new RecyclerAmenitiesChildAdapter(activity, new ArrayList<>(), view);

        bind.amenitiesChildRecyclerView.setHasFixedSize(true);
        bind.amenitiesChildRecyclerView.setLayoutManager(layoutManager);
        bind.amenitiesChildRecyclerView.setAdapter(childAdapter);
    }

    public void update(List<Amenities> amenities) {
        this.amenities.addAll(amenities);
        notifyDataSetChanged();
    }
}
