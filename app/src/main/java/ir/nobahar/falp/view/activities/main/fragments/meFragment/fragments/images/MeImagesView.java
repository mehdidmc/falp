package ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.images;

import java.util.List;

import ir.nobahar.falp.model.gallery.Gallery;

public interface MeImagesView {

    void showMessage(String message);

    void updateImages(List<Gallery> galleries);
}
