package ir.nobahar.falp.view.activities.login.fragments.loginPhone;

public interface LoginPhoneView {

    void showMessage(String message);

    String validatePhoneEntry();

    void setReceiveBtnEnabled(boolean enabled);
}
