package ir.nobahar.falp.view.activities.main.fragments.homeFragment;

import android.app.ActivityOptions;
import android.content.Intent;
import android.view.View;

import androidx.fragment.app.FragmentActivity;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.search.SearchActivity;
import ir.nobahar.falp.view.appDialog.AppDialog;
import ir.nobahar.falp.view.dialogProgress.DialogProgress;

public class MainHomeClickHandler {

    private final FragmentActivity activity;

    public MainHomeClickHandler(FragmentActivity activity) {
        this.activity = activity;
    }

    public void onCLickSearchTxv(View view) {
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(activity, view, activity.getString(R.string.search_transition));
        Intent searchIntent = new Intent(activity, SearchActivity.class);
        activity.startActivity(searchIntent, options.toBundle());
    }

    public void onCLickRegisterWork(View view) {
    }
}
