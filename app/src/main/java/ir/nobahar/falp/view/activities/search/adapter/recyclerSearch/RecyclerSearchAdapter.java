package ir.nobahar.falp.view.activities.search.adapter.recyclerSearch;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.businessDetail.BusinessDetailActivity;
import ir.nobahar.falp.databinding.SearchResultItemsBinding;
import ir.nobahar.falp.model.business.Business;
import ir.nobahar.falp.model.business.BusinessCategory;

public class RecyclerSearchAdapter extends RecyclerView.Adapter<RecyclerSearchViewHolder> {

    private final Context context;
    private final List<Business> businesses;

    public RecyclerSearchAdapter(Context context, List<Business> businesses) {
        this.context = context;
        this.businesses = businesses;
    }

    @NonNull
    @Override
    public RecyclerSearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        SearchResultItemsBinding binding = DataBindingUtil.inflate(inflater, R.layout.search_result_items, parent, false);
        return new RecyclerSearchViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerSearchViewHolder holder, int position) {
        Business business = businesses.get(position);
        List<BusinessCategory> categories = business.getCategories();

        String title = String.format("%s.%s", position + 1, business.getName());
        holder.binding.titleTxv.setText(title);

        holder.binding.setBusiness(business);

        StringBuilder category = new StringBuilder();
        for (BusinessCategory c : categories) {
            category.append(String.format("%s،", c.getTitle()));
        }

        holder.binding.setCategory(category.toString());

        switch (business.getCommentsScoreCounts()) {
            case "0":
                holder.binding.starsImg.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.starts_0, null));
                break;

            case "1":
                holder.binding.starsImg.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.starts_1, null));
                break;

            case "2":
                holder.binding.starsImg.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.starts_2, null));
                break;

            case "3":
                holder.binding.starsImg.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.starts_3, null));
                break;

            case "4":
                holder.binding.starsImg.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.starts_4, null));
                break;

            default:
                holder.binding.starsImg.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.starts_5, null));
                break;
        }

        if (!business.getGalleries().isEmpty()) {
            if (business.getGalleries().size() == 1) {
                Picasso.get().load(business.getGalleries().get(0).getImageUrl()).into(holder.binding.workImage1Img);
                holder.binding.workImage2Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
                holder.binding.workImage3Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
                holder.binding.workImage4Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));

            } else if (business.getGalleries().size() == 2) {
                Picasso.get().load(business.getGalleries().get(0).getImageUrl()).into(holder.binding.workImage1Img);
                Picasso.get().load(business.getGalleries().get(1).getImageUrl()).into(holder.binding.workImage2Img);
                holder.binding.workImage3Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
                holder.binding.workImage4Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));

            } else if (business.getGalleries().size() == 3) {
                Picasso.get().load(business.getGalleries().get(0).getImageUrl()).into(holder.binding.workImage1Img);
                Picasso.get().load(business.getGalleries().get(1).getImageUrl()).into(holder.binding.workImage2Img);
                Picasso.get().load(business.getGalleries().get(2).getImageUrl()).into(holder.binding.workImage3Img);
                holder.binding.workImage4Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));

            } else if (business.getGalleries().size() == 4) {
                Picasso.get().load(business.getGalleries().get(0).getImageUrl()).into(holder.binding.workImage1Img);
                Picasso.get().load(business.getGalleries().get(1).getImageUrl()).into(holder.binding.workImage2Img);
                Picasso.get().load(business.getGalleries().get(2).getImageUrl()).into(holder.binding.workImage3Img);
                Picasso.get().load(business.getGalleries().get(3).getImageUrl()).into(holder.binding.workImage4Img);
            }

        } else {
            holder.binding.workImage1Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
            holder.binding.workImage2Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
            holder.binding.workImage3Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
            holder.binding.workImage4Img.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.no_image_items, null));
        }

        switch (business.getPrice()) {
            case "0":
                holder.binding.dollarTextView.setText("$");
                break;

            case "1":
                holder.binding.dollarTextView.setText("$$");
                break;

            case "2":
                holder.binding.dollarTextView.setText("$$$");
                break;

            case "3":
                holder.binding.dollarTextView.setText("$$$$");
                break;
        }

        holder.itemView.setOnClickListener(view -> {
            Intent businessDetail = new Intent(context, BusinessDetailActivity.class);
            businessDetail.putExtra("business", business);
            context.startActivity(businessDetail);
        });
    }

    @Override
    public int getItemCount() {
        if (businesses.isEmpty()) return 0;
        else return businesses.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void update(List<Business> businesses) {
        this.businesses.addAll(businesses);
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void changeResult(List<Business> businesses) {
        this.businesses.clear();
        this.businesses.addAll(businesses);
        notifyDataSetChanged();
    }
}
