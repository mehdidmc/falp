package ir.nobahar.falp.view.activities.sendComments;

public interface SendCommentView {

    void showMessage(String message);

    void uploadImages(String commentId);
}
