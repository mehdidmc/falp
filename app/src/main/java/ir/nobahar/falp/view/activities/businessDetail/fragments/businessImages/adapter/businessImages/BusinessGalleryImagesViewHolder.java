package ir.nobahar.falp.view.activities.businessDetail.fragments.businessImages.adapter.businessImages;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.nobahar.falp.databinding.BusinessImagesPicsItemLayoutBinding;

public class BusinessGalleryImagesViewHolder extends RecyclerView.ViewHolder {

    public BusinessImagesPicsItemLayoutBinding bind;

    public BusinessGalleryImagesViewHolder(@NonNull BusinessImagesPicsItemLayoutBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
