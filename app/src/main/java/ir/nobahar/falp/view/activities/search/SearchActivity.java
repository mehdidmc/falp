package ir.nobahar.falp.view.activities.search;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.filters.FiltersActivity;
import ir.nobahar.falp.view.activities.search.adapter.recyclerSearch.RecyclerSearchAdapter;
import ir.nobahar.falp.view.activities.search.adapter.autoComplete.SearchAutoCompleteAdapter;
import ir.nobahar.falp.databinding.ActivitySearchBinding;
import ir.nobahar.falp.model.business.Business;
import ir.nobahar.falp.model.businessSearch.SearchModel;
import ir.nobahar.falp.util.Assistant;

import static ir.nobahar.falp.view.activities.search.adapter.autoComplete.SearchAutoCompleteAdapter.AUTO_COMPLETE_DELAY;
import static ir.nobahar.falp.view.activities.search.adapter.autoComplete.SearchAutoCompleteAdapter.TRIGGER_AUTO_COMPLETE;

public class SearchActivity extends AppCompatActivity implements SearchView, SwipeRefreshLayout.OnRefreshListener, TextWatcher, SearchClickHandler {

    private Handler handler;
    private SearchAutoCompleteAdapter searchAutoCompleteAdapter;
    private ActivitySearchBinding binding;

    private RecyclerSearchAdapter adapter;
    private SearchPresenter presenter;

    private SearchModel searchModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);

        presenter = new SearchPresenter(this, this);

        binding.setClickHandler(this);

        setupRecyclerView();
        setSearchFilters(null);
        setupAutoCompleteTextView();
        setupAutoCompleteSearchAction();

        presenter.searchByLocation(false);

        binding.recyclerRefreshLayout.setOnRefreshListener(this);
        binding.searchItemAutoComplete.addTextChangedListener(this);
    }

    //begin of Listener methods {
    @Override
    public void onCLickClearSearch(View view) {
        SearchModel filters = getSearchFilters();
        filters.setFindDesc("");
        filters.setStart("0");
        filters.setCount("20");
        setSearchFilters(filters);
        presenter.searchByLocation(false);

        binding.searchItemAutoComplete.clearListSelection();
        binding.searchItemAutoComplete.setText(null);
        binding.clearSearchImg.setVisibility(View.GONE);
    }

    @Override
    public void onCLickSearch(View view) {
        if (!binding.searchItemAutoComplete.getText().toString().isEmpty()) {
            SearchModel filters = getSearchFilters();
            String searchText = binding.searchItemAutoComplete.getText().toString();
            filters.setFindDesc(searchText);
            filters.setStart("0");
            filters.setCount("20");
            setSearchFilters(filters);
            presenter.searchByLocation(false);
        }
    }

    @Override
    public void onClickFilters(View view) {
        Intent intent = new Intent(this, FiltersActivity.class);

        String priceFilter = getSearchFilters().getPrice();
        String sortFilter = getSearchFilters().getSort();
        String openNow = getSearchFilters().getOpenNow();

        intent.putExtra("price_filter", priceFilter);
        intent.putExtra("sort_filter", sortFilter);
        intent.putExtra("open_now", openNow);

        startActivityForResult(intent, Assistant.LAUNCH_FILTER_ACTIVITY);
        overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_right);
    }

    @Override
    public void onClickOpenNow(View view) {
        SearchModel filter = getSearchFilters();
        int filterCount = filter.getFilterCount();

        if (!filter.getOpenNow().equals("1")) {
            filter.setOpenNow("1");
            ++filterCount;
            openNowButtonSelect(true);

        } else {
            filter.setOpenNow("");
            --filterCount;
            openNowButtonSelect(false);
        }

        filter.setFilterCount(filterCount, this);
        setSearchFilters(filter);
        presenter.searchByLocation(false);
    }

    @Override
    public void onClickExpandSort(View view) {
        if (binding.priceLayout.getVisibility() == View.VISIBLE)
            binding.priceLayout.setVisibility(View.GONE);

        if (binding.sortLayout.getVisibility() == View.GONE) binding.sortLayout.setVisibility(View.VISIBLE);
        else binding.sortLayout.setVisibility(View.GONE);

        TransitionManager.beginDelayedTransition(binding.expandableContainer, new AutoTransition());
    }

    @Override
    public void onCLickExpandPrice(View view) {
        if (binding.sortLayout.getVisibility() == View.VISIBLE)
            binding.sortLayout.setVisibility(View.GONE);

        if (binding.priceLayout.getVisibility() == View.GONE) binding.priceLayout.setVisibility(View.VISIBLE);
        else binding.priceLayout.setVisibility(View.GONE);

        TransitionManager.beginDelayedTransition(binding.expandableContainer, new AutoTransition());
    }

    @Override
    public void onClickPrice(View view) {
        SearchModel filter = getSearchFilters();
        int filterCount = filter.getFilterCount();
        String priceFilter = filter.getPrice();
        String allPrice = "0,1,2,3";

        if (view.getId() == R.id.oneDollarFilter_cardView) {
            if (binding.oneDollarFilterTxv.getCurrentTextColor() == getResources().getColor(R.color.black)) {
                priceButtonSelect("1", "black");
                ++filterCount;

                if (priceFilter.isEmpty()) priceFilter = "0";
                else priceFilter += ",0";

            } else {
                priceButtonSelect("1", "white");
                --filterCount;

                if (priceFilter.length() <= 1) priceFilter = allPrice;
                else priceFilter = priceFilter.replace(",0", "");
            }

        } else if (view.getId() == R.id.twoDollarFilter_cardView) {
            if (binding.twoDollarFilterTxv.getCurrentTextColor() == getResources().getColor(R.color.black)) {
                priceButtonSelect("2", "black");
                ++filterCount;

                if (priceFilter.isEmpty()) priceFilter = "1";
                else priceFilter += ",1";

            } else {
                priceButtonSelect("2", "white");
                --filterCount;

                if (priceFilter.length() <= 1) priceFilter = allPrice;
                else priceFilter = priceFilter.replace(",1", "");
            }

        } else if (view.getId() == R.id.threeDollarFilter_cardView) {
            if (binding.threeDollarFilterTxv.getCurrentTextColor() == getResources().getColor(R.color.black)) {
                priceButtonSelect("3", "black");
                ++filterCount;

                if (priceFilter.isEmpty()) priceFilter = "2";
                else priceFilter += ",2";

            } else {
                priceButtonSelect("3", "white");
                --filterCount;

                if (priceFilter.length() <= 1) priceFilter = allPrice;
                else priceFilter = priceFilter.replace(",2", "");
            }

        } else {
            if (binding.fourDollarFilterTxv.getCurrentTextColor() == getResources().getColor(R.color.black)) {
                priceButtonSelect("4", "black");
                ++filterCount;

                if (priceFilter.isEmpty()) priceFilter = "3";
                else priceFilter += ",3";

            } else {
                priceButtonSelect("4", "white");
                --filterCount;

                if (priceFilter.length() <= 1) priceFilter = allPrice;
                else priceFilter = priceFilter.replace(",3", "");
            }
        }

        filter.setFilterCount(filterCount, this);
        filter.setPrice(priceFilter);
        setSearchFilters(filter);
        presenter.searchByLocation(false);
    }

    @Override
    public void onCLickSort(View view) {
        SearchModel filter = getSearchFilters();
        String sort = "0";

        if (view.getId() == R.id.defaultSort_cardView) {
            sort = "0";
            sortButtonSelect(sort);

        } else if (view.getId() == R.id.mostRankSort_cardView) {
            sort = "3";
            sortButtonSelect(sort);

        } else if (view.getId() == R.id.mostCommentSort_cardView) {
            sort = "4";
            sortButtonSelect(sort);
        }

        filter.setSort(sort);
        setSearchFilters(filter);
        presenter.searchByLocation(false);
    }

    @Override
    public void onClickRemoveFilters(View view) {
        searchModel.setStart("0");
        searchModel.setCount("20");
        searchModel.setCityId("333");
        searchModel.setSort("0");
        searchModel.setPrice("");
        searchModel.setOpenNow("0");
        searchModel.setAttrs("");
        searchModel.setFilterCount(0, this);
        searchModel.setMaxLat(String.valueOf(SearchModel.DEFAULT_LAT + SearchModel.Y_ZOOM_12));
        searchModel.setMinLat(String.valueOf(SearchModel.DEFAULT_LAT - SearchModel.Y_ZOOM_12));
        searchModel.setMaxLng(String.valueOf(SearchModel.DEFAULT_LNG + SearchModel.X_ZOOM_12));
        searchModel.setMinLng(String.valueOf(SearchModel.DEFAULT_LNG - SearchModel.X_ZOOM_12));

        binding.oneDollarFilterCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
        binding.oneDollarFilterCardView.setStrokeColor(getResources().getColor(R.color.black));
        binding.oneDollarFilterTxv.setTextColor(getResources().getColor(R.color.black));

        binding.twoDollarFilterCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
        binding.twoDollarFilterCardView.setStrokeColor(getResources().getColor(R.color.black));
        binding.twoDollarFilterTxv.setTextColor(getResources().getColor(R.color.black));

        binding.threeDollarFilterCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
        binding.threeDollarFilterCardView.setStrokeColor(getResources().getColor(R.color.black));
        binding.threeDollarFilterTxv.setTextColor(getResources().getColor(R.color.black));

        binding.fourDollarFilterCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
        binding.fourDollarFilterCardView.setStrokeColor(getResources().getColor(R.color.black));
        binding.fourDollarFilterTxv.setTextColor(getResources().getColor(R.color.black));

        binding.openNowCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
        binding.openNowCardView.setStrokeColor(getResources().getColor(R.color.black));
        binding.openNowTextView.setTextColor(getResources().getColor(R.color.black));

        presenter.searchByLocation(false);
    }

    // } end of listener methods

    // view implement methods {
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateSearchResult(List<Business> businesses) {
        adapter.update(businesses);
    }

    @Override
    public void changeSearchResult(List<Business> businesses) {
        adapter.changeResult(businesses);
    }

    @Override
    public void setSearchFilters(SearchModel filters) {
        if (searchModel == null) {
            searchModel = new SearchModel();

            searchModel.setStart("0");
            searchModel.setCount("20");
            searchModel.setCityId("333");
            searchModel.setSort("0");
            searchModel.setPrice("");
            searchModel.setFilterCount(0, null);
            searchModel.setMaxLat(String.valueOf(SearchModel.DEFAULT_LAT + SearchModel.Y_ZOOM_12));
            searchModel.setMinLat(String.valueOf(SearchModel.DEFAULT_LAT - SearchModel.Y_ZOOM_12));
            searchModel.setMaxLng(String.valueOf(SearchModel.DEFAULT_LNG + SearchModel.X_ZOOM_12));
            searchModel.setMinLng(String.valueOf(SearchModel.DEFAULT_LNG - SearchModel.X_ZOOM_12));

        } else searchModel = filters;
    }

    @Override
    public SearchModel getSearchFilters() {
        return searchModel;
    }

    @Override
    public void setRefreshing(boolean refresh) {
        binding.recyclerRefreshLayout.setRefreshing(refresh);
    }

    @Override
    public void setAutoCompleteItems(List<String> searchItems) {
        searchAutoCompleteAdapter.setAutoCompleteItems(searchItems);
    }

    @Override
    public void onRefresh() {
        presenter.searchByLocation(true);
        binding.recyclerRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onFilterNumberChanged(int filterCount) {
        binding.filterNumberTxv.setText(String.valueOf(filterCount));

        if (filterCount > 0) {
            binding.filtersCountCardView.setVisibility(View.VISIBLE);
            binding.filtersCountCardView.setCardBackgroundColor(getResources().getColor(R.color.transparent_red_light));
            binding.filtersCountCardView.setStrokeColor(getResources().getColor(R.color.red));
            binding.filtersTxv.setTextColor(getResources().getColor(R.color.red));
            binding.filterNumberCardView.setCardBackgroundColor(getResources().getColor(R.color.red));
            binding.filterNumberCardView.setStrokeColor(getResources().getColor(R.color.red));
            binding.filterNumberTxv.setTextColor(getResources().getColor(R.color.white));

        } else
            binding.filtersCountCardView.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Assistant.LAUNCH_FILTER_ACTIVITY && resultCode == RESULT_OK) {
            if (data != null) {
                Bundle extras = data.getExtras();
                searchModel = (SearchModel) extras.getSerializable("backFilters");
                searchModel.setFilterCount(searchModel.getFilterCount(), this);
                presenter.searchByLocation(false);

                sortButtonSelect(searchModel.getSort());

                if (searchModel.getPrice().contains("0")) priceButtonSelect("1", "black");
                if (searchModel.getPrice().contains("1")) priceButtonSelect("2", "black");
                if (searchModel.getPrice().contains("2")) priceButtonSelect("3", "black");
                if (searchModel.getPrice().contains("3")) priceButtonSelect("4", "black");

                if (searchModel.getOpenNow().equals("1")) openNowButtonSelect(true);
            }
        }
    }

    // } end of view implement methods


    // begin of onTextChanged methods {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() >= 1) binding.clearSearchImg.setVisibility(View.VISIBLE);
        else binding.clearSearchImg.setVisibility(View.GONE);

        handler.removeMessages(TRIGGER_AUTO_COMPLETE);
        handler.sendEmptyMessageDelayed(TRIGGER_AUTO_COMPLETE, AUTO_COMPLETE_DELAY);
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
    // } end of onTextChanged methods

    //begin of private and inner methods {
    private void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new RecyclerSearchAdapter(this, new ArrayList<>());

        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.setLayoutManager(layoutManager);

        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (layoutManager.findLastVisibleItemPosition() == layoutManager.getItemCount() - 1) {
                    int lastStartItem = Integer.parseInt(searchModel.getStart());
                    presenter.updateRecyclerView(lastStartItem);
                }
            }
        });
    }

    private void setupAutoCompleteTextView() {
        searchAutoCompleteAdapter = new SearchAutoCompleteAdapter(this, android.R.layout.simple_dropdown_item_1line);
        binding.searchItemAutoComplete.setAdapter(searchAutoCompleteAdapter);

        binding.searchItemAutoComplete.setOnItemClickListener((parent, view, position, id) -> {
        });

        handler = new Handler(msg -> {
            if (msg.what == TRIGGER_AUTO_COMPLETE) {
                String searchTitle = binding.searchItemAutoComplete.getText().toString();
                presenter.searchByCategory(searchTitle);
            }
            return false;
        });
    }

    private void setupAutoCompleteSearchAction() {
        binding.searchItemAutoComplete.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (!binding.searchItemAutoComplete.getText().toString().isEmpty()) {
                    SearchModel filters = getSearchFilters();
                    String searchText = binding.searchItemAutoComplete.getText().toString();
                    filters.setFindDesc(searchText);
                    filters.setStart("0");
                    filters.setCount("20");
                    setSearchFilters(filters);
                    presenter.searchByLocation(false);
                }
            }
            return false;
        });
    }

    private void sortButtonSelect(String sort) {
        if (sort.equals("0")) {
            binding.defaultSortTxv.setTextColor(getResources().getColor(R.color.red));
            binding.defaultSortCardView.setCardBackgroundColor(getResources().getColor(R.color.transparent_red_light));
            binding.defaultSortCardView.setStrokeColor(getResources().getColor(R.color.red));

            binding.mostRankSortTxv.setTextColor(getResources().getColor(R.color.black));
            binding.mostRankSortCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            binding.mostRankSortCardView.setStrokeColor(getResources().getColor(R.color.black));

            binding.mostCommentSortTxv.setTextColor(getResources().getColor(R.color.black));
            binding.mostCommentSortCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            binding.mostCommentSortCardView.setStrokeColor(getResources().getColor(R.color.black));

        } else if (sort.equals("3")) {
            binding.mostRankSortTxv.setTextColor(getResources().getColor(R.color.red));
            binding.mostRankSortCardView.setCardBackgroundColor(getResources().getColor(R.color.transparent_red_light));
            binding.mostRankSortCardView.setStrokeColor(getResources().getColor(R.color.red));

            binding.defaultSortTxv.setTextColor(getResources().getColor(R.color.black));
            binding.defaultSortCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            binding.defaultSortCardView.setStrokeColor(getResources().getColor(R.color.black));

            binding.mostCommentSortTxv.setTextColor(getResources().getColor(R.color.black));
            binding.mostCommentSortCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            binding.mostCommentSortCardView.setStrokeColor(getResources().getColor(R.color.black));

        } else {
            binding.mostCommentSortTxv.setTextColor(getResources().getColor(R.color.red));
            binding.mostCommentSortCardView.setCardBackgroundColor(getResources().getColor(R.color.transparent_red_light));
            binding.mostCommentSortCardView.setStrokeColor(getResources().getColor(R.color.red));

            binding.defaultSortTxv.setTextColor(getResources().getColor(R.color.black));
            binding.defaultSortCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            binding.defaultSortCardView.setStrokeColor(getResources().getColor(R.color.black));

            binding.mostRankSortTxv.setTextColor(getResources().getColor(R.color.black));
            binding.mostRankSortCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            binding.mostRankSortCardView.setStrokeColor(getResources().getColor(R.color.black));
        }
    }

    private void priceButtonSelect(String price, String color) {
        switch (price) {
            case "1":
                if (color.equals("black")) {
                    binding.oneDollarFilterCardView.setCardBackgroundColor(getResources().getColor(R.color.transparent_red_light));
                    binding.oneDollarFilterCardView.setStrokeColor(getResources().getColor(R.color.red));
                    binding.oneDollarFilterTxv.setTextColor(getResources().getColor(R.color.red));

                } else if (color.equals("white")) {
                    binding.oneDollarFilterCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
                    binding.oneDollarFilterCardView.setStrokeColor(getResources().getColor(R.color.black));
                    binding.oneDollarFilterTxv.setTextColor(getResources().getColor(R.color.black));
                }
                break;

            case "2":
                if (color.equals("black")) {
                    binding.twoDollarFilterCardView.setCardBackgroundColor(getResources().getColor(R.color.transparent_red_light));
                    binding.twoDollarFilterCardView.setStrokeColor(getResources().getColor(R.color.red));
                    binding.twoDollarFilterTxv.setTextColor(getResources().getColor(R.color.red));

                } else if (color.equals("white")) {
                    binding.twoDollarFilterCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
                    binding.twoDollarFilterCardView.setStrokeColor(getResources().getColor(R.color.black));
                    binding.twoDollarFilterTxv.setTextColor(getResources().getColor(R.color.black));
                }
                break;

            case "3":
                if (color.equals("black")) {
                    binding.threeDollarFilterCardView.setCardBackgroundColor(getResources().getColor(R.color.transparent_red_light));
                    binding.threeDollarFilterCardView.setStrokeColor(getResources().getColor(R.color.red));
                    binding.threeDollarFilterTxv.setTextColor(getResources().getColor(R.color.red));

                } else if (color.equals("white")) {
                    binding.threeDollarFilterCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
                    binding.threeDollarFilterCardView.setStrokeColor(getResources().getColor(R.color.black));
                    binding.threeDollarFilterTxv.setTextColor(getResources().getColor(R.color.black));
                }
                break;

            default:
                if (color.equals("black")) {
                    binding.fourDollarFilterCardView.setCardBackgroundColor(getResources().getColor(R.color.transparent_red_light));
                    binding.fourDollarFilterCardView.setStrokeColor(getResources().getColor(R.color.red));
                    binding.fourDollarFilterTxv.setTextColor(getResources().getColor(R.color.red));

                } else if (color.equals("white")) {
                    binding.fourDollarFilterCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
                    binding.fourDollarFilterCardView.setStrokeColor(getResources().getColor(R.color.black));
                    binding.fourDollarFilterTxv.setTextColor(getResources().getColor(R.color.black));
                }
                break;
        }
    }

    private void openNowButtonSelect(boolean isOpen) {
        if (isOpen) {
            binding.openNowCardView.setCardBackgroundColor(getResources().getColor(R.color.transparent_red_light));
            binding.openNowCardView.setStrokeColor(getResources().getColor(R.color.red));
            binding.openNowTextView.setTextColor(getResources().getColor(R.color.red));

        } else {
            binding.openNowCardView.setCardBackgroundColor(getResources().getColor(R.color.white));
            binding.openNowCardView.setStrokeColor(getResources().getColor(R.color.black));
            binding.openNowTextView.setTextColor(getResources().getColor(R.color.black));
        }
    }
    // } end of private and inner methods
}