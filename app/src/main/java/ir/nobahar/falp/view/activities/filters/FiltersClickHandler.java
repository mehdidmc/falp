package ir.nobahar.falp.view.activities.filters;

import android.view.View;

public interface FiltersClickHandler {

    void onClickBack(View view);

    void onCLickSortItem(View view);

    void onClickPriceItem(View view);

    void onClickSearch(View view);
}
