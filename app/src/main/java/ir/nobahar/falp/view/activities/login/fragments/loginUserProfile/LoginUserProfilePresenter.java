package ir.nobahar.falp.view.activities.login.fragments.loginUserProfile;

import android.content.Intent;
import android.net.Uri;

import androidx.fragment.app.FragmentActivity;

import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashMap;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.main.MainActivity;
import ir.nobahar.falp.model.user.User;
import ir.nobahar.falp.util.Assistant;
import ir.nobahar.falp.util.JwtTokens;
import ir.nobahar.falp.util.LoginChecking;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.CommonServices;
import ir.nobahar.falp.webServices.services.LoginServices;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginUserProfilePresenter {

    private final FragmentActivity activity;
    private final LoginUserProfileView view;

    public LoginUserProfilePresenter(FragmentActivity activity, LoginUserProfileView view) {
        this.activity = activity;
        this.view = view;
    }

    public void sendUserData(Uri avatarUri) {
        User user = view.checkInputValidation();

        if (user != null) {
            HashMap<String, String> body = new HashMap<>();
            body.put("first_name", user.getFirstName());
            body.put("last_name", user.getLastName());
            body.put("country_id", user.getCountryId());
            body.put("city_id", user.getCityId());
            body.put("phone", user.getPhone());
            body.put("email", user.getEmail());
            body.put("password", user.getPassword());
            body.put("device", Assistant.DEVICE_KEY);
            body.put("client_name", Assistant.get().getDeviceName());

            LoginServices services = RetrofitInstance.getInstance().create(LoginServices.class);
            Call<JsonObject> call = services.sendUserData(body);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        String message = response.body().get("message").getAsString();
                        int status = response.body().get("status").getAsInt();
                        String jwt = response.headers().get("authorization");

                        if (status == 200) {
                            view.showMessage(message);
                            JwtTokens.getInstance(activity).setJwtTokens(jwt);
                            LoginChecking.getInstance(activity).setLoginMode(true);
                            uploadAvatar(avatarUri);

                            Intent mainIntent = new Intent(activity, MainActivity.class);
                            activity.startActivity(mainIntent);
                            activity.finish();

                        } else if (status == 400) {
                            view.showMessage(message);
                        }
                    }
                }

                @Override
                public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                    view.showMessage(activity.getString(R.string.retrofit_onFailure));
                    t.printStackTrace();
                }
            });
        }
    }

    private void uploadAvatar(Uri avatarUri) {
        if (view.avatarImageValidation()) {
            String jwt = JwtTokens.getInstance(activity).getJwtTokens();
            File avatarFile = new File(avatarUri.getPath());
            RequestBody requestBody = RequestBody.create(MultipartBody.FORM, avatarFile);
            MultipartBody.Part avatarPart = MultipartBody.Part.createFormData("avatar", avatarFile.getName(), requestBody);

            CommonServices services = RetrofitInstance.getInstance().create(CommonServices.class);
            Call<ResponseBody> call = services.uploadAvatar(jwt, avatarPart);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
//                    if (response.isSuccessful()) Toast.makeText(activity, "OK", Toast.LENGTH_SHORT).show();
//                    else Toast.makeText(activity, "Not OK", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                    view.showMessage(activity.getString(R.string.retrofit_onFailure));
                    t.printStackTrace();
                }
            });
        }
     }
}
