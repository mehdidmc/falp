package ir.nobahar.falp.view.activities.businessDetail.fragments.businessImages;

import android.view.View;

public interface BusinessImagesClickHandler {

    void onClickSendImage(View view);
}
