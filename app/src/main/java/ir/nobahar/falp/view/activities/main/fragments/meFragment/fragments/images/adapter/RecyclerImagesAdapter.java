package ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.images.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.MeImagesItemsBinding;
import ir.nobahar.falp.model.gallery.Gallery;

public class RecyclerImagesAdapter extends RecyclerView.Adapter<RecyclerImagesViewHolder> {

    private final Context context;
    private final List<Gallery> galleries;

    public RecyclerImagesAdapter(Context context, List<Gallery> galleries) {
        this.context = context;
        this.galleries = galleries;
    }

    @NonNull
    @Override
    public RecyclerImagesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        MeImagesItemsBinding binding = DataBindingUtil.inflate(inflater, R.layout.me_images_items, parent, false);
        return new RecyclerImagesViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerImagesViewHolder holder, int position) {
        Gallery gallery = galleries.get(position);
        Picasso.get().load(gallery.getImageUrl()).into(holder.binding.meImagesImg);
    }

    @Override
    public int getItemCount() {
        if (galleries.isEmpty()) return 0;
        else return galleries.size();
    }

    public void update(List<Gallery> galleries) {
        if (galleries != null) this.galleries.addAll(galleries);
        notifyDataSetChanged();
    }
}
