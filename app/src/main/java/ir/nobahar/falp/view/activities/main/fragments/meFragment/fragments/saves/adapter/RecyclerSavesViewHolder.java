package ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.saves.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.nobahar.falp.databinding.MeSavedItemsBinding;

public class RecyclerSavesViewHolder extends RecyclerView.ViewHolder {

    public MeSavedItemsBinding binding;

    public RecyclerSavesViewHolder(@NonNull MeSavedItemsBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
