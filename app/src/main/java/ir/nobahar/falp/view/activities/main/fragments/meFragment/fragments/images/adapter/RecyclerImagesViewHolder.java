package ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.images.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.nobahar.falp.databinding.MeImagesItemsBinding;

public class RecyclerImagesViewHolder extends RecyclerView.ViewHolder {

    public MeImagesItemsBinding binding;

    public RecyclerImagesViewHolder(@NonNull MeImagesItemsBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
