package ir.nobahar.falp.view.activities.businessDetail.fragments.businessFullView;

import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.FragmentBusinessFullViewBinding;
import ir.nobahar.falp.model.business.Business;
import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryImages;
import ir.nobahar.falp.model.scoreDetail.ScoreDetail;
import ir.nobahar.falp.util.LoginChecking;

public class BusinessFullViewFragment extends Fragment implements BusinessFullViewView {

    private Business business;
    private boolean isBusinessBookmarked = false;

    public BusinessFullViewFragment() {
        // Required empty public constructor
    }

    public BusinessFullViewFragment(Business business) {
        this.business = business;
    }

    private FragmentBusinessFullViewBinding bind;
    private BusinessFullViewPresenter presenter;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bind = DataBindingUtil.inflate(inflater, R.layout.fragment_business_full_view, container, false);
        presenter = new BusinessFullViewPresenter(getActivity(), this);

        BusinessFullViewClickHandler clickHandler = new BusinessFullViewClickHandler(getActivity(), bind, business, presenter, this);
        bind.setClickHandler(clickHandler);

        presenter.getBusinessInfo(business.getUrl(), "", "");
        presenter.getScoreDetail(business.getUrl());
        presenter.getImportantImages(business.getBizHashId(), "");

        if (LoginChecking.getInstance(getContext()).getLoginMode())
            presenter.checkBusinessBookmark(business.getBizHashId());

        return bind.getRoot();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setCircleProgress(double progress, String peopleCounts) {
        int percent = (int) (progress * 100) / 5;
        bind.circleProgressBar.setProgress(percent);
        bind.progressPercentTxv.setText(String.format("%s%%", percent));
        bind.progressRankTxv.setText(String.valueOf(progress));
        bind.rankTxv.setText(String.valueOf(progress));
        bind.rankViewerTxv.setText(peopleCounts);
    }

    @Override
    public void setScoreDetail(List<ScoreDetail> scoreDetail) {
        int score1 = Integer.parseInt(scoreDetail.get(0).getScoreCount());
        int score2 = Integer.parseInt(scoreDetail.get(1).getScoreCount());
        int score3 = Integer.parseInt(scoreDetail.get(2).getScoreCount());
        int score4 = Integer.parseInt(scoreDetail.get(3).getScoreCount());
        int score5 = Integer.parseInt(scoreDetail.get(4).getScoreCount());
        int allScoreSum = score1 + score2 + score3 + score4 + score5;

        int score1Percent = 0;
        int score2Percent = 0;
        int score3Percent = 0;
        int score4Percent = 0;
        int score5Percent = 0;

        try {
            score1Percent = (score1 * 100) / allScoreSum;
            score2Percent = (score2 * 100) / allScoreSum;
            score3Percent = (score3 * 100) / allScoreSum;
            score4Percent = (score4 * 100) / allScoreSum;
            score5Percent = (score5 * 100) / allScoreSum;

        } catch (ArithmeticException ex) {
            ex.printStackTrace();
        }

        bind.progressScore1.setProgress(score1Percent);
        bind.progressScore2.setProgress(score2Percent);
        bind.progressScore3.setProgress(score3Percent);
        bind.progressScore4.setProgress(score4Percent);
        bind.progressScore5.setProgress(score5Percent);
    }

    @Override
    public void setBestImages(List<GalleryImages> images) {
        if (images != null && !images.isEmpty()) {
            if (images.size() == 4) {
                Picasso.get().load(images.get(0).getImageUrl()).into(bind.bestImage1);
                Picasso.get().load(images.get(1).getImageUrl()).into(bind.bestImage2);
                Picasso.get().load(images.get(2).getImageUrl()).into(bind.bestImage3);
                Picasso.get().load(images.get(3).getImageUrl()).into(bind.bestImage4);

            } else if (images.size() ==3) {
                Picasso.get().load(images.get(0).getImageUrl()).into(bind.bestImage1);
                Picasso.get().load(images.get(1).getImageUrl()).into(bind.bestImage2);
                Picasso.get().load(images.get(2).getImageUrl()).into(bind.bestImage3);
                bind.bestImage4.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.no_image_items, null));

            } else if (images.size() == 2) {
                Picasso.get().load(images.get(0).getImageUrl()).into(bind.bestImage1);
                Picasso.get().load(images.get(1).getImageUrl()).into(bind.bestImage2);
                bind.bestImage3.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.no_image_items, null));
                bind.bestImage4.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.no_image_items, null));

            } else if (images.size() == 1) {
                Picasso.get().load(images.get(0).getImageUrl()).into(bind.bestImage1);
                bind.bestImage2.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.no_image_items, null));
                bind.bestImage3.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.no_image_items, null));
                bind.bestImage4.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.no_image_items, null));

            } else {
                bind.bestImage1.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.no_image_items, null));
                bind.bestImage2.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.no_image_items, null));
                bind.bestImage3.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.no_image_items, null));
                bind.bestImage4.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.no_image_items, null));
            }

        } else noImportantImage();
    }

    @Override
    public void noImportantImage() {
        bind.bestImageLayout.setVisibility(View.GONE);
    }

    @Override
    public void setBusinessBookmark(boolean isSaved) {
        this.isBusinessBookmarked = isSaved;
        if (this.isBusinessBookmarked)
            bind.bookmarkImv.setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.ic_bookmark_full));
    }

    @Override
    public boolean isBusinessBookmarked() {
        return isBusinessBookmarked;
    }
}