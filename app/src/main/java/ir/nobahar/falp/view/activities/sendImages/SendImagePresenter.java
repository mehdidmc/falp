package ir.nobahar.falp.view.activities.sendImages;

import android.app.Activity;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import ir.nobahar.falp.R;
import ir.nobahar.falp.util.Assistant;
import ir.nobahar.falp.util.JwtTokens;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.SendImageServices;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendImagePresenter {

    private final SendImageView view;
    private final Activity activity;

    public SendImagePresenter(SendImageView view, Activity activity) {
        this.view = view;
        this.activity = activity;
    }

    public void uploadImage(Uri imageUri, String caption, String businessId) {
        File imageFile = new File(imageUri.getPath());
        RequestBody requestBody = RequestBody.create(MultipartBody.FORM, imageFile);
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("image", imageFile.getName(), requestBody);

        String jwt = JwtTokens.getInstance(activity).getJwtTokens();

        RequestBody captionBody = RequestBody.create(MultipartBody.FORM, caption);
        RequestBody userIdBody = RequestBody.create(MultipartBody.FORM, Assistant.get().getUserId(activity));
        RequestBody businessIDBody = RequestBody.create(MultipartBody.FORM, businessId);
        RequestBody galleryIdBody = RequestBody.create(MultipartBody.FORM, "1");

        SendImageServices services = RetrofitInstance.getInstance().create(SendImageServices.class);
        Call<ResponseBody> call = services.uploadImage(jwt, imagePart, captionBody, userIdBody, businessIDBody, galleryIdBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    view.uploadImages();
                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                t.printStackTrace();
                view.showMessage(activity.getString(R.string.retrofit_onFailure));
            }
        });
    }
}
