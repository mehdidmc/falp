package ir.nobahar.falp.view.activities.businessDetail.fragments.businessComments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.businessDetail.fragments.businessComments.adapter.RecyclerBusinessCommentAdapter;
import ir.nobahar.falp.databinding.FragmentBusinessCommentsBinding;
import ir.nobahar.falp.model.business.Business;
import ir.nobahar.falp.model.comments.Comments;

public class BusinessCommentsFragment extends Fragment implements BusinessCommentView {

    private Business business;

    public BusinessCommentsFragment() {
        // Required empty public constructor
    }

    public BusinessCommentsFragment(Business business) {
        this.business = business;
    }

    private FragmentBusinessCommentsBinding bind;

    private BusinessCommentPresenter presenter;
    private RecyclerBusinessCommentAdapter commentAdapter;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bind = DataBindingUtil.inflate(inflater, R.layout.fragment_business_comments, container, false);

        BusinessCommentClickHandler clickHandler = new BusinessCommentClickHandler(getActivity(), bind, business);
        bind.setClickHandler(clickHandler);

        presenter = new BusinessCommentPresenter(getActivity(), this);
        presenter.getComments(business.getUrl(), "", "0", "20");
        setupCommentRecyclerView();
        return bind.getRoot();
    }

    private void setupCommentRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        commentAdapter = new RecyclerBusinessCommentAdapter(getContext(), new ArrayList<>());

        bind.commentsRecyclerView.setLayoutManager(layoutManager);
        bind.commentsRecyclerView.setAdapter(commentAdapter);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateComments(List<Comments> comments) {
        if (comments != null) commentAdapter.update(comments);
        else bind.noCommentTxv.setVisibility(View.VISIBLE);
    }
}