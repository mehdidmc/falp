package ir.nobahar.falp.view.activities.main.fragments.meFragment;

public interface MainMeView {

    void showUserInformation(String fullName, String gallery, String comments);
}
