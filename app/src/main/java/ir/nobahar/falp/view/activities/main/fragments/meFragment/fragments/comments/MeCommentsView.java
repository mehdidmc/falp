package ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.comments;

public interface MeCommentsView {

    void showMessage(String message);
}
