package ir.nobahar.falp.view.activities.sendComments;

import android.app.Activity;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.HashMap;

import ir.nobahar.falp.R;
import ir.nobahar.falp.util.Assistant;
import ir.nobahar.falp.util.JwtTokens;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.SendCommentServices;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendCommentPresenter {

    private final Activity activity;
    private final SendCommentView view;

    public SendCommentPresenter(Activity activity, SendCommentView view) {
        this.activity = activity;
        this.view = view;
    }

    public void sendComment(String comment, String score, String businessUrl) {
        String jwt = JwtTokens.getInstance(activity).getJwtTokens();

        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", Assistant.get().getUserId(activity));
        body.put("score", score);
        body.put("comments", comment);
        body.put("business_url", businessUrl);
        body.put("parent_id", "0");

        SendCommentServices services = RetrofitInstance.getInstance().create(SendCommentServices.class);
        Call<JsonObject> call = services.sendComment(jwt, body);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String commentId = response.body().get("comments_id").getAsString();
                    String message = response.body().get("message").getAsString();
                    int status = response.body().get("status").getAsInt();

                    if (status == 200) {
                        view.showMessage(message);
                        view.uploadImages(commentId);
                    }
                    else view.showMessage(activity.getString(R.string.retrofit_response_error));
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                view.showMessage(activity.getString(R.string.retrofit_onFailure));
                t.printStackTrace();
            }
        });
    }

    public void sendImages(Uri imageUri, String businessId, String commentId) {
        File imageFile = new File(imageUri.getPath());
        RequestBody imageRequest = RequestBody.create(MultipartBody.FORM, imageFile);
        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("image", imageFile.getName(), imageRequest);

        String jwt = JwtTokens.getInstance(activity).getJwtTokens();

        RequestBody userIdRequest = RequestBody.create(MultipartBody.FORM, Assistant.get().getUserId(activity));
        RequestBody businessIdRequest = RequestBody.create(MultipartBody.FORM, businessId);
        RequestBody galleryIdRequest = RequestBody.create(MultipartBody.FORM, "1");
        RequestBody commentIdRequest = RequestBody.create(MultipartBody.FORM, commentId);

        SendCommentServices services = RetrofitInstance.getInstance().create(SendCommentServices.class);
        Call<ResponseBody> call = services.sendImages(jwt, imagePart, userIdRequest, businessIdRequest, commentIdRequest, galleryIdRequest);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    view.uploadImages(commentId);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                view.showMessage(activity.getString(R.string.retrofit_onFailure));
                t.printStackTrace();
            }
        });
    }
}
