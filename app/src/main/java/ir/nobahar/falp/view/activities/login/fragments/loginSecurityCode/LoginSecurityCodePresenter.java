package ir.nobahar.falp.view.activities.login.fragments.loginSecurityCode;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import androidx.fragment.app.FragmentActivity;

import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Locale;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.login.fragments.loginUserProfile.LoginUserProfileFragment;
import ir.nobahar.falp.view.activities.main.MainActivity;
import ir.nobahar.falp.util.Assistant;
import ir.nobahar.falp.util.JwtTokens;
import ir.nobahar.falp.util.LoginChecking;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.LoginServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginSecurityCodePresenter {

    private final FragmentActivity activity;
    private final LoginSecurityCodeView view;

    private CountDownTimer countDownTimer = null;

    public LoginSecurityCodePresenter(FragmentActivity activity, LoginSecurityCodeView view) {
        this.activity = activity;
        this.view = view;
    }

    public void loginToApp() {
        view.setLoginBtnEnabled(false);
        String code = view.getCodeEntry();
        String phone = view.getPhone();

        if (code != null) {
            HashMap<String, String> body = new HashMap<>();
            body.put("active_code", code);
            body.put("phone", phone);
            body.put("device", Assistant.DEVICE_KEY);
            body.put("client_name", Assistant.get().getDeviceName());

            LoginServices loginServices = RetrofitInstance.getInstance().create(LoginServices.class);
            Call<JsonObject> call = loginServices.activeCode(body);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        String message = response.body().get("message").getAsString();
                        String jwt = response.headers().get("authorization");
                        int status = response.body().get("status").getAsInt();
                        boolean member = response.body().get("member").getAsBoolean();

                        if (status == 200) {
                            view.showMessage(message);

                            if (!member) {
                                LoginUserProfileFragment fragment = new LoginUserProfileFragment();
                                Bundle argument = new Bundle();
                                argument.putString("phone_number", view.getPhone());
                                fragment.setArguments(argument);
                                Assistant.get().setFragmentToLayoutWithAnimation(fragment, activity.getSupportFragmentManager(), R.id.frameLayout_loginActivity);

                            } else {
                                JwtTokens.getInstance(activity).setJwtTokens(jwt);
                                Intent mainActivityIntent = new Intent(activity, MainActivity.class);
                                LoginChecking.getInstance(activity).setLoginMode(true);
                                activity.startActivity(mainActivityIntent);
                                activity.finish();
                            }

                        } else if (status == 400) {
                            view.showMessage(message);
                            view.setLoginBtnEnabled(true);
                        }
                    } else {
                        view.setLoginBtnEnabled(true);
                        view.showMessage(activity.getString(R.string.response_null));
                    }
                }

                @Override
                public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                    view.setLoginBtnEnabled(true);
                    view.showMessage(activity.getString(R.string.retrofit_onFailure));
                    t.printStackTrace();
                }
            });

        } else {
            view.setLoginBtnEnabled(true);
        }
    }

    public void resendSecurityCode() {
        view.setResendBtnEnabled(false);
        String phone = view.getPhone();

        if (!phone.isEmpty()) {
            HashMap<String, String> body = new HashMap<>();
            body.put("phone", phone);

            LoginServices loginServices = RetrofitInstance.getInstance().create(LoginServices.class);
            Call<JsonObject> call = loginServices.checkPhoneNumber(body);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        String message = response.body().get("message").getAsString();
                        int status = response.body().get("status").getAsInt();

                        if (status == 200) {
                            view.showMessage(message);
                            startCountDownTimer();
                        }
                        else if (status == 400) {
                            view.showMessage(message);
                            view.setResendBtnEnabled(true);
                        }
                    }
                }

                @Override
                public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                    view.setResendBtnEnabled(true);
                    view.showMessage(activity.getString(R.string.retrofit_onFailure));
                    t.printStackTrace();
                }
            });

        } else view.setResendBtnEnabled(true);
    }

    private void createCounterDownTimer() {
        countDownTimer = new CountDownTimer(120000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds %= 60;

                String time = String.format(Locale.getDefault(), "%02d", seconds) +
                        " : " + String.format(Locale.getDefault(), "%02d", minutes);

                view.setCounterDownTxv(time);
            }

            @Override
            public void onFinish() {
                view.setResendBtnEnabled(true);
            }

        };
    }

    public void startCountDownTimer() {
        if (countDownTimer == null) createCounterDownTimer();
        countDownTimer.start();
    }

    public void stopCounterDownTimer() {
        countDownTimer.cancel();
    }
}
