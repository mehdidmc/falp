package ir.nobahar.falp.view.activities.businessDetail.fragments.businessComments.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.nobahar.falp.databinding.BusinessCommentsItemsBinding;

public class RecyclerBusinessCommentViewHolder extends RecyclerView.ViewHolder {

    public BusinessCommentsItemsBinding bind;

    public RecyclerBusinessCommentViewHolder(@NonNull BusinessCommentsItemsBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
