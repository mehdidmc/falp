package ir.nobahar.falp.view.activities.search;

import java.util.List;

import ir.nobahar.falp.model.business.Business;
import ir.nobahar.falp.model.businessSearch.SearchModel;

public interface SearchView {

    void showMessage(String message);

    void updateSearchResult(List<Business> businesses);

    void changeSearchResult(List<Business> businesses);

    void setSearchFilters(SearchModel filters);

    SearchModel getSearchFilters();

    void setRefreshing(boolean refresh);

    void setAutoCompleteItems(List<String> searchItems);

    void onFilterNumberChanged(int filterCount);
}
