package ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.images;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.images.adapter.RecyclerImagesAdapter;
import ir.nobahar.falp.databinding.FragmentMeImagesBinding;
import ir.nobahar.falp.model.gallery.Gallery;

public class MeImagesFragment extends Fragment implements MeImagesView {

    private RecyclerImagesAdapter adapter;
    private GridLayoutManager layoutManager;
    private MeImagesPresenter presenter;

    private int startItem = 0;

    public MeImagesFragment() {
        // Required empty public constructor
    }

    private FragmentMeImagesBinding binding;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_me_images, container, false);

        setupRecyclerView();

        presenter = new MeImagesPresenter(getActivity(), this);
        presenter.getImageGallery(startItem);

        endOfRecyclerView();

        return binding.getRoot();
    }

    private void setupRecyclerView() {
        layoutManager = new GridLayoutManager(getContext(), 3);
        adapter = new RecyclerImagesAdapter(getContext(), new ArrayList<>());

        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.setAdapter(adapter);
    }

    private void endOfRecyclerView() {
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (layoutManager.findLastVisibleItemPosition() == layoutManager.getItemCount() -1) {
                    int tempStartItem = startItem;
                    startItem = presenter.updateRecyclerViewItems(tempStartItem);
                }
            }
        });
    }

    @Override
    public void showMessage(String message) {
        Snackbar snackbar = Snackbar.make(binding.containerLayout, message, Snackbar.LENGTH_LONG);

        ViewCompat.setLayoutDirection(snackbar.getView(), ViewCompat.LAYOUT_DIRECTION_RTL);

        TextView snackText = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
        Typeface font = ResourcesCompat.getFont(Objects.requireNonNull(getContext()), R.font.sans_medium);
        snackText.setTypeface(font);

        snackbar.show();
    }

    @Override
    public void updateImages(List<Gallery> galleries) {
        adapter.update(galleries);
    }
}