package ir.nobahar.falp.view.activities.filters.adapters.amenitiesAdapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import ir.nobahar.falp.databinding.FatherAmenitiesTitleItemsBinding;

public class RecyclerAmenitiesViewHolder extends RecyclerView.ViewHolder {

    public FatherAmenitiesTitleItemsBinding bind;

    public RecyclerAmenitiesViewHolder(@NonNull @NotNull FatherAmenitiesTitleItemsBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
