package ir.nobahar.falp.view.activities.businessDetail.fragments.businessFullView;

import java.util.List;

import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryImages;
import ir.nobahar.falp.model.scoreDetail.ScoreDetail;

public interface BusinessFullViewView {

    void showMessage(String message);

    void setCircleProgress(double progress, String peopleCounts);

    void setScoreDetail(List<ScoreDetail> scoreDetail);

    void setBestImages(List<GalleryImages> images);

    void noImportantImage();

    void setBusinessBookmark(boolean isSaved);

    boolean isBusinessBookmarked();
}
