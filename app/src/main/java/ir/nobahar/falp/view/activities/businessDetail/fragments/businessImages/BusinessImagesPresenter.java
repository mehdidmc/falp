package ir.nobahar.falp.view.activities.businessDetail.fragments.businessImages;

import android.app.Activity;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryFilters;
import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryImagesResponse;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.BusinessDetailServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessImagesPresenter {

    private final Activity activity;
    private final BusinessImagesView view;

    public BusinessImagesPresenter(Activity activity, BusinessImagesView view) {
        this.activity = activity;
        this.view = view;
    }

    public void getGalleryImagesFilter(String businessId, String userId) {
        BusinessDetailServices services = RetrofitInstance.getInstance().create(BusinessDetailServices.class);
        Call<List<GalleryFilters>> call = services.getGalleryFilters(businessId, userId);
        call.enqueue(new Callback<List<GalleryFilters>>() {
            @Override
            public void onResponse(@NotNull Call<List<GalleryFilters>> call, @NotNull Response<List<GalleryFilters>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    view.updateFilterAdapter(response.body());
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<GalleryFilters>> call, @NotNull Throwable t) {
                view.showMessage(activity.getResources().getString(R.string.retrofit_onFailure));
                t.printStackTrace();
            }
        });
    }

    public void getBusinessImages(String businessId, String categoryId, String start, String count) {
        BusinessDetailServices services = RetrofitInstance.getInstance().create(BusinessDetailServices.class);
        Call<GalleryImagesResponse> call = services.getGalleryImages(businessId, categoryId, start, count);
        call.enqueue(new Callback<GalleryImagesResponse>() {
            @Override
            public void onResponse(@NotNull Call<GalleryImagesResponse> call, @NotNull Response<GalleryImagesResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    view.updateImagesAdapter(response.body().getImages());
                }
            }

            @Override
            public void onFailure(@NotNull Call<GalleryImagesResponse> call, @NotNull Throwable t) {
                view.showMessage(activity.getResources().getString(R.string.retrofit_onFailure));
                t.printStackTrace();
            }
        });
    }
}
