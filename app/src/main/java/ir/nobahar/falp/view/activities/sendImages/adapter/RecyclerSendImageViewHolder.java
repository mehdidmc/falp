package ir.nobahar.falp.view.activities.sendImages.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.nobahar.falp.databinding.RecyclerSendImagesItemsBinding;

public class RecyclerSendImageViewHolder extends RecyclerView.ViewHolder {

    public RecyclerSendImagesItemsBinding bind;

    public RecyclerSendImageViewHolder(@NonNull RecyclerSendImagesItemsBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
