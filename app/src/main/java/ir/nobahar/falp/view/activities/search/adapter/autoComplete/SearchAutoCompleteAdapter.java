package ir.nobahar.falp.view.activities.search.adapter.autoComplete;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class SearchAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

    public static final int TRIGGER_AUTO_COMPLETE = 100;
    public static final long AUTO_COMPLETE_DELAY = 300;

    private final List<String> items;

    public SearchAutoCompleteAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        items = new ArrayList<>();
    }

    public void setAutoCompleteItems(List<String> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return items.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
         return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null) {
                    results.values = items;
                    results.count = items.size();
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) notifyDataSetChanged();
                else notifyDataSetInvalidated();
            }
        };
    }
}
