package ir.nobahar.falp.view.activities.businessDetail.fragments.businessImages;

import java.util.List;

import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryFilters;
import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryImages;

public interface BusinessImagesView {

    void showMessage(String message);

    void updateFilterAdapter(List<GalleryFilters> filters);

    void updateImagesAdapter(List<GalleryImages> images);
}
