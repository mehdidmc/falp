package ir.nobahar.falp.view.activities.main.fragments.meFragment.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.comments.MeCommentsFragment;
import ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.images.MeImagesFragment;
import ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.saves.MeSavesFragment;

public class MePagerAdapter extends FragmentStateAdapter {

    public MePagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0: return new MeSavesFragment();
            case 1: return new MeCommentsFragment();
            case 2: return new MeImagesFragment();
            default:return null;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
