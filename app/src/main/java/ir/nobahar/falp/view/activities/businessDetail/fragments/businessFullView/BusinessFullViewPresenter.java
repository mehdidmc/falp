package ir.nobahar.falp.view.activities.businessDetail.fragments.businessFullView;

import android.app.Activity;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.model.business.Business;
import ir.nobahar.falp.model.businessDetail.BusinessInfoResponse;
import ir.nobahar.falp.model.businessDetail.fullView.ImportantImagesResponse;
import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryImages;
import ir.nobahar.falp.model.scoreDetail.ScoreDetail;
import ir.nobahar.falp.util.Assistant;
import ir.nobahar.falp.util.JwtTokens;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.BookmarkServices;
import ir.nobahar.falp.webServices.services.BusinessDetailServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessFullViewPresenter {

    private final Activity activity;
    private final BusinessFullViewView view;

    public BusinessFullViewPresenter(Activity activity, BusinessFullViewView view) {
        this.activity = activity;
        this.view = view;
    }

    public void getBusinessInfo(String url, String userId, String bizId) {
        String jwt = JwtTokens.getInstance(activity).getJwtTokens();

        BusinessDetailServices services = RetrofitInstance.getInstance().create(BusinessDetailServices.class);
        Call<BusinessInfoResponse> call = services.getBusinessInfo(jwt, url, userId, bizId);
        call.enqueue(new Callback<BusinessInfoResponse>() {
            @Override
            public void onResponse(@NotNull Call<BusinessInfoResponse> call, @NotNull Response<BusinessInfoResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Business business = response.body().getBusinesses().get(0);
                    view.setCircleProgress(Double.parseDouble(business.getCommentsScoreCounts()), business.getCommentsCount());
                }
            }

            @Override
            public void onFailure(@NotNull Call<BusinessInfoResponse> call, @NotNull Throwable t) {
                activity.getResources().getString(R.string.retrofit_onFailure);
                t.printStackTrace();
            }
        });
    }

    public void getScoreDetail(String url) {
        BusinessDetailServices services = RetrofitInstance.getInstance().create(BusinessDetailServices.class);
        Call<List<ScoreDetail>> call = services.getScoreDetail(url);
        call.enqueue(new Callback<List<ScoreDetail>>() {
            @Override
            public void onResponse(@NotNull Call<List<ScoreDetail>> call, @NotNull Response<List<ScoreDetail>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    view.setScoreDetail(response.body());
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<ScoreDetail>> call, @NotNull Throwable t) {
                view.showMessage(activity.getResources().getString(R.string.retrofit_onFailure));
                t.printStackTrace();
            }
        });
    }

    public void getImportantImages(String businessId, String userVisitedId) {
        BusinessDetailServices services = RetrofitInstance.getInstance().create(BusinessDetailServices.class);
        Call<ImportantImagesResponse> call = services.getImportantImages(businessId, userVisitedId);
        call.enqueue(new Callback<ImportantImagesResponse>() {
            @Override
            public void onResponse(@NotNull Call<ImportantImagesResponse> call, @NotNull Response<ImportantImagesResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<GalleryImages> images = response.body().getImages();
                    view.setBestImages(images);
                } else {
                    Toast.makeText(activity, "Response Error", Toast.LENGTH_SHORT).show();
                    view.noImportantImage();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ImportantImagesResponse> call, @NotNull Throwable t) {
                //view.showMessage(activity.getResources().getString(R.string.retrofit_onFailure));
                t.printStackTrace();
                view.noImportantImage();
            }
        });
    }

    public void bookmarkBusiness(String businessId) {
        String jwt = JwtTokens.getInstance(activity).getJwtTokens();
        String userId = Assistant.get().getUserId(activity);

        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", userId);
        body.put("business_id", businessId);
        body.put("note", "this is just a note");

        BookmarkServices services = RetrofitInstance.getInstance().create(BookmarkServices.class);
        Call<JsonObject> call = services.setBookmark(jwt, body);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (response.isSuccessful() && response.body() != null) {
                    String id = response.body().get("id").getAsString();
                    String message = response.body().get("message").getAsString();
                    int status = response.body().get("status").getAsInt();

                    if (status == 200) {
                        view.showMessage(message);
                        view.setBusinessBookmark(true);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                view.showMessage(activity.getString(R.string.retrofit_onFailure));
                t.printStackTrace();
            }
        });
    }

    public void checkBusinessBookmark(String businessId) {
        String jwt = JwtTokens.getInstance(activity).getJwtTokens();
        String userId = Assistant.get().getUserId(activity);

        BookmarkServices services = RetrofitInstance.getInstance().create(BookmarkServices.class);
        Call<JsonObject> call = services.checkBookmark(jwt, userId, businessId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if (response.isSuccessful() && response.body() != null) {
                    //String businessSaveId = response.body().get("business_save_id").getAsString();
                    boolean isSaved = response.body().get("saved").getAsBoolean();

                    view.setBusinessBookmark(isSaved);
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                view.showMessage(activity.getString(R.string.retrofit_onFailure));
                t.printStackTrace();
            }
        });
    }
}
