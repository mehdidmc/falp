package ir.nobahar.falp.view.appDialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.ImageView;

import androidx.databinding.DataBindingUtil;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.DialogContentViewBinding;

public class AppDialog {

    private static AppDialog instance = null;

    private Dialog dialog;

    private DialogContentViewBinding bind;

    private AppDialog() {
    }

    public static synchronized AppDialog get() {
        if (instance == null) instance = new AppDialog();
        return instance;
    }

    public void create(Context context, String title, String content) {
        dialog = new Dialog(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        bind = DataBindingUtil.inflate(inflater, R.layout.dialog_content_view, null, false);

        dialog.setCancelable(false);
        dialog.setContentView(bind.getRoot());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        bind.titleDialogTxv.setText(title);
        bind.contentDialogTxv.setText(content);

        bind.negativeBtn.setOnClickListener(view -> dialog.dismiss());
    }

    public Button getPositiveButton() {
        return bind.positiveBtn;
    }

    public Button getNegativeButton() {
        return bind.negativeBtn;
    }

    public ImageView getIcon() {
        return bind.dialogIconImg;
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }
}
