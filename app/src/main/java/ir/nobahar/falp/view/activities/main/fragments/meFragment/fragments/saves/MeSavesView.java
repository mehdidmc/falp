package ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.saves;

import java.util.List;

import ir.nobahar.falp.model.business.Business;

public interface MeSavesView {

    void showMessage(String message);

    void updateSavedBusinesses(List<Business> businesses);
}
