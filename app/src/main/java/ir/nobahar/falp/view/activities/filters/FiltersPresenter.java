package ir.nobahar.falp.view.activities.filters;

import android.app.Activity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.model.amenities.AmenitiesResponse;
import ir.nobahar.falp.model.amenities.TopAmenities;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.SearchServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FiltersPresenter {

    private final Activity activity;
    private final FiltersView view;

    public FiltersPresenter(Activity activity, FiltersView view) {
        this.activity = activity;
        this.view = view;
    }

    public void getAmenities(String category, String userId, String findDesc) {
        String cityId = activity.getString(R.string.tehran_city_code);

        SearchServices search = RetrofitInstance.getInstance().create(SearchServices.class);
        Call<AmenitiesResponse> call = search.getAmenities(category, cityId, userId, findDesc);
        call.enqueue(new Callback<AmenitiesResponse>() {
            @Override
            public void onResponse(@NotNull Call<AmenitiesResponse> call, @NotNull Response<AmenitiesResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    //TopAmenities init {
                    List<TopAmenities> topAmenities = new ArrayList<>();

                    TopAmenities newAndHotAmenity = new TopAmenities();
                    newAndHotAmenity.setId(String.valueOf(101));
                    newAndHotAmenity.setTitle("داغ و جدید");
                    topAmenities.add(newAndHotAmenity);

                    TopAmenities openNowAmenity = new TopAmenities();
                    openNowAmenity.setId(String.valueOf(201));
                    openNowAmenity.setTitle("الان باز است");
                    topAmenities.add(openNowAmenity);

                    topAmenities.addAll(response.body().getTopAmenities());
                    view.recommendedFilters(topAmenities);
                    // }

                    //Common Amenities {
                    view.amenitiesFilters(response.body().getAmenities());
                }
            }

            @Override
            public void onFailure(@NotNull Call<AmenitiesResponse> call, @NotNull Throwable t) {
                view.showMessage(activity.getString(R.string.retrofit_onFailure));
                t.printStackTrace();
            }
        });
    }
}
