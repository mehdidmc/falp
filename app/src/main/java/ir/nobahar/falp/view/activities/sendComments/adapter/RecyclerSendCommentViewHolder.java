package ir.nobahar.falp.view.activities.sendComments.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.nobahar.falp.databinding.SendCommentImagesItemLayoutBinding;

public class RecyclerSendCommentViewHolder extends RecyclerView.ViewHolder {

    public SendCommentImagesItemLayoutBinding bind;

    public RecyclerSendCommentViewHolder(@NonNull SendCommentImagesItemLayoutBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
