package ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation.adapter.BusinessHours;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.nobahar.falp.databinding.RecyclerHoursBusinessInfoBinding;

public class RecyclerBusinessHourViewHolder extends RecyclerView.ViewHolder {

    public RecyclerHoursBusinessInfoBinding bind;

    public RecyclerBusinessHourViewHolder(@NonNull RecyclerHoursBusinessInfoBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
