package ir.nobahar.falp.view.activities.login.fragments.loginSecurityCode;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.FragmentLoginSecurityCodeBinding;

public class LoginSecurityCodeFragment extends Fragment implements LoginSecurityCodeView, TextWatcher {

    private FragmentLoginSecurityCodeBinding binding;
    private LoginSecurityCodePresenter presenter;

    public LoginSecurityCodeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login_security_code, container, false);

        presenter = new LoginSecurityCodePresenter(getActivity(), this);
        LoginSecurityCodeClickHandler clickHandler = new LoginSecurityCodeClickHandler(getActivity(), presenter);

        binding.setClickHandler(clickHandler);

        presenter.startCountDownTimer();

        binding.securityCode1Edt.addTextChangedListener(this);
        binding.securityCode2Edt.addTextChangedListener(this);
        binding.securityCode3Edt.addTextChangedListener(this);
        binding.securityCode4Edt.addTextChangedListener(this);
        binding.securityCode5Edt.addTextChangedListener(this);
        binding.securityCode6Edt.addTextChangedListener(this);

        binding.phoneNumberTxv.setText(getPhone());

        return binding.getRoot();
    }

    @Override
    public void setResendBtnEnabled(boolean enabled) {
        if (enabled) {
            binding.resendCodeTxv.setEnabled(true);
            binding.resendCodeTxv.setTextColor(getResources().getColor(R.color.colorAccent));

        } else {
            binding.resendCodeTxv.setEnabled(false);
            binding.resendCodeTxv.setTextColor(getResources().getColor(R.color.gray));
        }
    }

    @Override
    public void setCounterDownTxv(String time) {
        binding.counterDownSecondsTxv.setText(time);
    }

    @Override
    public String getCodeEntry() {
        String entry = binding.securityCode1Edt.getText().toString();
        entry += binding.securityCode2Edt.getText().toString();
        entry += binding.securityCode3Edt.getText().toString();
        entry += binding.securityCode4Edt.getText().toString();
        entry += binding.securityCode5Edt.getText().toString();
        entry += binding.securityCode6Edt.getText().toString();

        if (entry.isEmpty()) {
            showMessage(Objects.requireNonNull(getContext()).getString(R.string.btn_security_code_empty));
            return null;

        } else if (entry.length() < 4) {
            showMessage(Objects.requireNonNull(getContext()).getString(R.string.btn_security_length));
            return null;
        }

        return entry;
    }

    @Override
    public String getPhone() {
        return Objects.requireNonNull(getArguments()).getString("phone_number");
    }

    @Override
    public void showMessage(String message) {
        Snackbar snackbar = Snackbar.make(binding.containerLayout, message, Snackbar.LENGTH_LONG);

        ViewCompat.setLayoutDirection(snackbar.getView(), ViewCompat.LAYOUT_DIRECTION_RTL);

        TextView snackText = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
        Typeface font = ResourcesCompat.getFont(Objects.requireNonNull(getContext()), R.font.sans_medium);
        snackText.setTypeface(font);

        snackbar.show();
    }

    @Override
    public void setLoginBtnEnabled(boolean enabled) {
        binding.confirmCodeFab.setEnabled(enabled);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.stopCounterDownTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.stopCounterDownTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.startCountDownTimer();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (binding.securityCode1Edt.isFocused()) {
            if (binding.securityCode1Edt.getText().toString().trim().length() == 0) {
                binding.securityCode1Edt.requestFocus();

            } else {
                binding.securityCode1Edt.clearFocus();
                binding.securityCode2Edt.requestFocus();
            }

        } else if (binding.securityCode2Edt.isFocused()) {
            if (binding.securityCode2Edt.getText().toString().trim().length() == 0) {
                binding.securityCode2Edt.clearFocus();
                binding.securityCode1Edt.requestFocus();

            } else {
                binding.securityCode2Edt.clearFocus();
                binding.securityCode3Edt.requestFocus();
            }

        } else if (binding.securityCode3Edt.isFocused()) {
            if (binding.securityCode3Edt.getText().toString().trim().length() == 0) {
                binding.securityCode3Edt.clearFocus();
                binding.securityCode2Edt.requestFocus();

            } else {
                binding.securityCode3Edt.clearFocus();
                binding.securityCode4Edt.requestFocus();
            }

        } else if (binding.securityCode4Edt.isFocused()) {
            if (binding.securityCode4Edt.getText().toString().trim().length() == 0) {
                binding.securityCode4Edt.clearFocus();
                binding.securityCode3Edt.requestFocus();

            } else {
                binding.securityCode4Edt.clearFocus();
                binding.securityCode5Edt.requestFocus();
            }

        } else if (binding.securityCode5Edt.isFocused()) {
            if (binding.securityCode5Edt.getText().toString().trim().length() == 0) {
                binding.securityCode5Edt.clearFocus();
                binding.securityCode4Edt.requestFocus();

            } else {
                binding.securityCode5Edt.clearFocus();
                binding.securityCode6Edt.requestFocus();
            }

        } else if (binding.securityCode6Edt.isFocused()) {
            if (binding.securityCode6Edt.getText().toString().trim().length() == 0) {
                binding.securityCode6Edt.clearFocus();
                binding.securityCode5Edt.requestFocus();

            } else {
                binding.securityCode6Edt.clearFocus();
                binding.confirmCodeFab.requestFocus();
            }
        }
    }
}