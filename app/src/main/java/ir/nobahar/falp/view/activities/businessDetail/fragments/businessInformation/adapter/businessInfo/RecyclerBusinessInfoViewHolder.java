package ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation.adapter.businessInfo;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.nobahar.falp.databinding.BusinessMoreInfoItemsBinding;

public class RecyclerBusinessInfoViewHolder extends RecyclerView.ViewHolder {

    public BusinessMoreInfoItemsBinding bind;

    public RecyclerBusinessInfoViewHolder(@NonNull BusinessMoreInfoItemsBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
