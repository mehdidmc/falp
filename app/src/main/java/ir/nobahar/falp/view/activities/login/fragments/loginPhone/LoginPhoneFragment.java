package ir.nobahar.falp.view.activities.login.fragments.loginPhone;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.FragmentLoginPhoneBinding;

public class LoginPhoneFragment extends Fragment implements LoginPhoneView, TextWatcher {

    private FragmentLoginPhoneBinding binding;

    public LoginPhoneFragment() {
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login_phone, container, false);

        LoginPhonePresenter presenter = new LoginPhonePresenter(this, getActivity());
        LoginPhoneClickHandler clickHandler = new LoginPhoneClickHandler(presenter);

        binding.setClickHandler(clickHandler);

        Objects.requireNonNull(binding.phoneTxi.getEditText()).addTextChangedListener(this);

        return binding.getRoot();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String validatePhoneEntry() {
        String phone = Objects.requireNonNull(binding.phoneTxi.getEditText()).getText().toString();

        if (phone.isEmpty()) {
            binding.phoneTxi.setError(Objects.requireNonNull(getContext()).getResources().getString(R.string.empty_phone));
            return null;

        } else if (phone.length() <= 10 || !phone.startsWith("09")) {
            binding.phoneTxi.setError(Objects.requireNonNull(getContext()).getString(R.string.wrong_phone));
            return null;

        } else return phone;
    }

    @Override
    public void setReceiveBtnEnabled(boolean enabled) {
        binding.confirmPhoneFab.setEnabled(enabled);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        binding.phoneTxi.setError(null);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}