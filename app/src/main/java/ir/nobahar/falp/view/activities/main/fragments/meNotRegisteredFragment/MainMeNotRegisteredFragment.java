package ir.nobahar.falp.view.activities.main.fragments.meNotRegisteredFragment;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.FragmentMainMeNotRegisteredBinding;

public class MainMeNotRegisteredFragment extends Fragment {

    public MainMeNotRegisteredFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentMainMeNotRegisteredBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_me_not_registered, container, false);
        MainMeNotRegisteredClickHandler clickHandler = new MainMeNotRegisteredClickHandler(getActivity());
        binding.setClickHandler(clickHandler);
        return binding.getRoot();
    }
}