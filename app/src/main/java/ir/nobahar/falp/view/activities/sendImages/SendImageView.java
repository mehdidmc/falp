package ir.nobahar.falp.view.activities.sendImages;

public interface SendImageView {

    void showMessage(String message);

    void uploadImages();
}
