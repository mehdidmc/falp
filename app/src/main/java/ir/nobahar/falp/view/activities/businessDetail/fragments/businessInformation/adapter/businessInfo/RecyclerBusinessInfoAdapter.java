package ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation.adapter.businessInfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.BusinessMoreInfoItemsBinding;

public class RecyclerBusinessInfoAdapter extends RecyclerView.Adapter<RecyclerBusinessInfoViewHolder> {

    private final Context context;
    private final String[] title;
    private final int[] icons;

    public RecyclerBusinessInfoAdapter(Context context) {
        this.context = context;

        title = context.getResources().getStringArray(R.array.business_info_titles);

        icons = new int[] {R.drawable.phone,
                R.drawable.message,
                R.drawable.fork,
                R.drawable.website,
                R.drawable.instagram,
                R.drawable.telegram};
    }

    @NonNull
    @Override
    public RecyclerBusinessInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        BusinessMoreInfoItemsBinding bind = DataBindingUtil.inflate(inflater, R.layout.business_more_info_items, parent, false);
        return new RecyclerBusinessInfoViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerBusinessInfoViewHolder holder, int position) {
        String title = this.title[position];
        int icon = this.icons[position];

        holder.bind.titleTxv.setText(title);
        holder.bind.iconImv.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), icon, null));
    }

    @Override
    public int getItemCount() {
        return title.length;
    }
}
