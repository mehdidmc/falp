package ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation.adapter.businessAmenities;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.nobahar.falp.databinding.BusinessInfoAmenitiesItemsBinding;

public class RecyclerBusinessAmenitiesViewHolder extends RecyclerView.ViewHolder {

    public BusinessInfoAmenitiesItemsBinding bind;

    public RecyclerBusinessAmenitiesViewHolder(@NonNull BusinessInfoAmenitiesItemsBinding bind) {
        super(bind.getRoot());
        this.bind = bind;
    }
}
