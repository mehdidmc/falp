package ir.nobahar.falp.view.activities.businessDetail.fragments.businessFullView;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;

import ir.nobahar.falp.R;
import ir.nobahar.falp.model.business.Business;
import ir.nobahar.falp.util.LoginChecking;
import ir.nobahar.falp.view.activities.login.LoginActivity;
import ir.nobahar.falp.view.activities.sendComments.SendCommentsActivity;
import ir.nobahar.falp.databinding.FragmentBusinessFullViewBinding;
import ir.nobahar.falp.view.activities.sendImages.SendImagesActivity;
import ir.nobahar.falp.view.appDialog.AppDialog;

public class BusinessFullViewClickHandler {

    private final Activity activity;
    private final FragmentBusinessFullViewBinding bind;
    private final Business business;
    private final BusinessFullViewPresenter presenter;
    private final BusinessFullViewView businessView;

    public BusinessFullViewClickHandler(Activity activity, FragmentBusinessFullViewBinding bind, Business business, BusinessFullViewPresenter presenter, BusinessFullViewView businessView) {
        this.activity = activity;
        this.bind = bind;
        this.business = business;
        this.presenter = presenter;
        this.businessView = businessView;
    }

    public void onClickSendImage(View view) {
        if (LoginChecking.getInstance(activity).getLoginMode()) {
            Intent intent = new Intent(activity, SendImagesActivity.class);
            intent.putExtra("business_id", business.getBizHashId());
            intent.putExtra("business_name", business.getName());
            activity.startActivity(intent);

        } else {
            AppDialog dialog = AppDialog.get();
            dialog.create(
                    activity,
                    activity.getString(R.string.no_login_title),
                    activity.getString(R.string.no_login_content));

            dialog.getPositiveButton().setText(activity.getString(R.string.positive_login));
            dialog.getPositiveButton().setOnClickListener(v -> {
                Intent intent = new Intent(activity, LoginActivity.class);
                activity.startActivity(intent);
                dialog.dismiss();
            });

            dialog.show();
        }
    }

    public void sendComment(View view) {
        if (LoginChecking.getInstance(activity).getLoginMode()) {
            Intent intent = new Intent(activity, SendCommentsActivity.class);
            intent.putExtra("business_id", business.getBizHashId());
            intent.putExtra("business_name", business.getName());
            intent.putExtra("business_url", business.getUrl());
            intent.putExtra("comment_rate", 0);
            activity.startActivity(intent);

        } else {
            AppDialog dialog = AppDialog.get();
            dialog.create(
                    activity,
                    activity.getString(R.string.no_login_title),
                    activity.getString(R.string.no_login_content));

            dialog.getPositiveButton().setText(activity.getString(R.string.positive_login));
            dialog.getPositiveButton().setOnClickListener(v -> {
                Intent intent = new Intent(activity, LoginActivity.class);
                activity.startActivity(intent);
                dialog.dismiss();
            });

            dialog.show();
        }
    }

    public void onClickSendRate(View view) {
        int commentRate = 0;

        if (view.getId() == R.id.firstStar_imv) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star0, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star0, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star0, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star0, null));

            commentRate = 1;

        } else if (view.getId() == R.id.secondStar_imv) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star2, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star0, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star0, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star0, null));

            commentRate = 2;

        } else if (view.getId() == R.id.thirdStar_imv) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star2, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star3, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star0, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star0, null));

            commentRate = 3;

        } else if (view.getId() == R.id.fourthStar_imv) {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star2, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star3, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star4, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star0, null));

            commentRate = 4;

        } else {
            bind.firstStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star1, null));
            bind.secondStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star2, null));
            bind.thirdStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star3, null));
            bind.fourthStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star4, null));
            bind.fifthStarImv.setImageDrawable(ResourcesCompat.getDrawable(activity.getResources(), R.drawable.big_circle_star5, null));

            commentRate = 5;
        }

        Intent intent = new Intent(activity, SendCommentsActivity.class);
        int finalCommentRate = commentRate;
        new Handler().postDelayed(() -> {
            if (LoginChecking.getInstance(activity).getLoginMode()) {
                intent.putExtra("business_id", business.getBizHashId());
                intent.putExtra("business_name", business.getName());
                intent.putExtra("business_url", business.getUrl());
                intent.putExtra("comment_rate", finalCommentRate);
                activity.startActivity(intent);

            } else {
                AppDialog dialog = AppDialog.get();
                dialog.create(
                        activity,
                        activity.getString(R.string.no_login_title),
                        activity.getString(R.string.no_login_content));

                dialog.getPositiveButton().setText(activity.getString(R.string.positive_login));
                dialog.getPositiveButton().setOnClickListener(v -> {
                    Intent loginIntent = new Intent(activity, LoginActivity.class);
                    activity.startActivity(loginIntent);
                    dialog.dismiss();
                });

                dialog.show();
            }
        } , 500);
    }

    public void onClickBookmarkBusiness(View view) {
        if (!businessView.isBusinessBookmarked()) {
            if (LoginChecking.getInstance(activity).getLoginMode())
                presenter.bookmarkBusiness(business.getBizHashId());

            else {
                AppDialog dialog = AppDialog.get();
                dialog.create(activity,
                        activity.getString(R.string.no_login_title),
                        activity.getString(R.string.no_login_content));

                dialog.getPositiveButton().setOnClickListener(v -> {
                    Intent intent = new Intent(activity, LoginActivity.class);
                    activity.startActivity(intent);
                    dialog.dismiss();
                });

                dialog.show();
            }
        } else {
            businessView.showMessage("قبلا ثبت شده است!!");
        }
    }
}
