package ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation;

import ir.nobahar.falp.model.business.Business;

public interface BusinessInfoView {

    void showMessage(String message);

    void setInfo(Business business);
}
