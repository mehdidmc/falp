package ir.nobahar.falp.view.activities.main.fragments.homeFragment;

import androidx.fragment.app.FragmentActivity;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import ir.nobahar.falp.R;
import ir.nobahar.falp.util.Assistant;
import ir.nobahar.falp.util.JwtTokens;
import ir.nobahar.falp.util.LoginChecking;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.MainServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainHomePresenter {

    private final FragmentActivity activity;

    private final MainHomeView view;

    public MainHomePresenter(MainHomeView view, FragmentActivity activity) {
        this.view = view;
        this.activity = activity;
    }

    public void getSliderImage() {
        String hashId = "";
        String city = activity.getString(R.string.tehran_city_code);

        boolean isLogin = LoginChecking.getInstance(activity).getLoginMode();
        if (isLogin) {
            String jwt = JwtTokens.getInstance(activity).getJwtTokens();
            String decodedJwt = Assistant.get().decodeJwt(jwt);
            JsonObject jsonObject = new Gson().fromJson(decodedJwt, JsonObject.class);
            hashId = jsonObject.getAsJsonObject("data").get("id").getAsString();
        }

        MainServices services = RetrofitInstance.getInstance().create(MainServices.class);
        Call<JsonObject> call = services.sliderImages(city, hashId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                if (response.isSuccessful() && response.body() != null) {
                    int status = response.body().get("status").getAsInt();
                    String imageUri = response.body().getAsJsonArray("data").get(0).getAsJsonObject().get("file_address").getAsString();

                    if (status == 200) view.setSlideUrlImage(imageUri);
                    else view.setDefaultSliderImages();

                } else view.setDefaultSliderImages();
            }

            @Override
            public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                view.showMessage(activity.getString(R.string.retrofit_onFailure));
                view.setDefaultSliderImages();
                t.printStackTrace();
            }
        });
    }
}
