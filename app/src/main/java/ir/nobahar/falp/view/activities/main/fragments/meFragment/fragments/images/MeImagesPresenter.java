package ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.images;

import androidx.fragment.app.FragmentActivity;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import ir.nobahar.falp.R;
import ir.nobahar.falp.model.gallery.GalleryResponse;
import ir.nobahar.falp.util.Assistant;
import ir.nobahar.falp.util.JwtTokens;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.MainServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeImagesPresenter {

    private final FragmentActivity activity;
    private final MeImagesView view;

    public MeImagesPresenter(FragmentActivity activity, MeImagesView view) {
        this.activity = activity;
        this.view = view;
    }

    public void getImageGallery(int startItem) {
        String jwt = JwtTokens.getInstance(activity).getJwtTokens();
        String decodedJwt = Assistant.get().decodeJwt(jwt);
        JsonObject jsonObject = new Gson().fromJson(decodedJwt, JsonObject.class);
        String hashId = jsonObject.getAsJsonObject("data").get("id").getAsString();

        MainServices services = RetrofitInstance.getInstance().create(MainServices.class);
        Call<GalleryResponse> call = services.getUserGalleries(hashId, startItem, 5);
        call.enqueue(new Callback<GalleryResponse>() {
            @Override
            public void onResponse(Call<GalleryResponse> call, Response<GalleryResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    view.updateImages(response.body().getGalleries());
                }
            }

            @Override
            public void onFailure(Call<GalleryResponse> call, Throwable t) {
                view.showMessage(activity.getString(R.string.retrofit_onFailure));
                t.printStackTrace();
            }
        });
    }

    public int updateRecyclerViewItems(int lastStartItem) {
        int currentStartItem = lastStartItem + 5;
        getImageGallery(currentStartItem);
        return currentStartItem;
    }
}
