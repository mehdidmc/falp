package ir.nobahar.falp.view.activities.filters.adapters.amenitiesChildAdapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.filters.FiltersView;
import ir.nobahar.falp.databinding.ChildAmenitiesContentItemBinding;
import ir.nobahar.falp.model.amenities.AmenitiesChild;

public class RecyclerAmenitiesChildAdapter extends RecyclerView.Adapter<RecyclerAmenitiesChildViewHolder> {

    private final Activity activity;
    private final List<AmenitiesChild> amenitiesChildren;
    private final FiltersView view;

    public RecyclerAmenitiesChildAdapter(Activity activity, List<AmenitiesChild> amenitiesChildren, FiltersView view) {
        this.activity = activity;
        this.amenitiesChildren = amenitiesChildren;
        this.view = view;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerAmenitiesChildViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        ChildAmenitiesContentItemBinding bind = DataBindingUtil.inflate(inflater, R.layout.child_amenities_content_item, parent, false);
        return new RecyclerAmenitiesChildViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerAmenitiesChildViewHolder holder, int position) {
        AmenitiesChild amenitiesChild = amenitiesChildren.get(position);
        holder.bind.childContentSwitch.setText(amenitiesChild.getTitle());

        holder.bind.childContentSwitch.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) view.setAttrsId(amenitiesChild.getId());
        });
    }

    @Override
    public int getItemCount() {
        if (amenitiesChildren.isEmpty()) return 0;
        return amenitiesChildren.size();
    }

    public void update(List<AmenitiesChild> amenitiesChildren) {
        this.amenitiesChildren.addAll(amenitiesChildren);
    }
}
