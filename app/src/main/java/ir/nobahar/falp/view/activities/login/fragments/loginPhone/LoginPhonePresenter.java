package ir.nobahar.falp.view.activities.login.fragments.loginPhone;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.login.fragments.loginSecurityCode.LoginSecurityCodeFragment;
import ir.nobahar.falp.util.Assistant;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.LoginServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPhonePresenter {

    private final LoginPhoneView view;
    private final FragmentActivity activity;

    public LoginPhonePresenter(LoginPhoneView view, FragmentActivity activity) {
        this.view = view;
        this.activity = activity;
    }

    public void checkPhoneNumber() {
        view.setReceiveBtnEnabled(false);
        String phone = view.validatePhoneEntry();

        if (phone != null) {
            HashMap<String, String> body = new HashMap<>();
            body.put("phone", phone);

            LoginServices loginServices = RetrofitInstance.getInstance().create(LoginServices.class);
            Call<JsonObject> call = loginServices.checkPhoneNumber(body);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(@NotNull Call<JsonObject> call, @NotNull Response<JsonObject> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        String message = response.body().get("message").getAsString();
                        String status = response.body().get("status").getAsString();

                        if (status.equals("200")) {
                            view.showMessage(message);

                            LoginSecurityCodeFragment securityCodeFragment = new LoginSecurityCodeFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("phone_number", phone);
                            securityCodeFragment.setArguments(bundle);
                            Assistant.get().setFragmentToLayoutWithAnimation(securityCodeFragment, activity.getSupportFragmentManager(), R.id.frameLayout_loginActivity);

                            view.setReceiveBtnEnabled(true);

                        } else if (status.equals("400")) {
                            view.showMessage(message);
                            view.setReceiveBtnEnabled(true);
                        }
                    }
                }

                @Override
                public void onFailure(@NotNull Call<JsonObject> call, @NotNull Throwable t) {
                    view.setReceiveBtnEnabled(true);
                    view.showMessage(activity.getString(R.string.retrofit_onFailure));
                    t.printStackTrace();
                }
            });

        } else view.setReceiveBtnEnabled(true);
    }
}
