package ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.saves;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.saves.adapter.RecyclerSavesAdapter;
import ir.nobahar.falp.databinding.FragmentMeSavesBinding;
import ir.nobahar.falp.model.business.Business;

public class MeSavesFragment extends Fragment implements MeSavesView {

    private RecyclerSavesAdapter recyclerAdapter;
    private LinearLayoutManager layoutManager;
    private MeSavesPresenter presenter;

    private int lastStartItem = 0;

    public MeSavesFragment() {
        // Required empty public constructor
    }

    private FragmentMeSavesBinding binding;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_me_saves, container, false);

        setupRecyclerView();

        presenter = new MeSavesPresenter(getActivity(), this);
        presenter.getSavedBusinesses(lastStartItem);

        endOfRecyclerViewItem();

        return binding.getRoot();
    }

    private void setupRecyclerView() {
        layoutManager = new LinearLayoutManager(getContext());
        recyclerAdapter = new RecyclerSavesAdapter(getContext(), new ArrayList<>());

        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.setAdapter(recyclerAdapter);
    }

    private void endOfRecyclerViewItem() {
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (layoutManager.findLastVisibleItemPosition() == layoutManager.getItemCount() -1) {
                    int tempStartItem = lastStartItem;
                    lastStartItem = presenter.updateRecyclerViewItem(tempStartItem);
                }
            }
        });
    }

    @Override
    public void showMessage(String message) {
        Snackbar snackbar = Snackbar.make(binding.containerLayout, message, Snackbar.LENGTH_LONG);

        ViewCompat.setLayoutDirection(snackbar.getView(), ViewCompat.LAYOUT_DIRECTION_RTL);

        TextView snackText = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
        Typeface font = ResourcesCompat.getFont(Objects.requireNonNull(getContext()), R.font.sans_medium);
        snackText.setTypeface(font);

        snackbar.show();
    }

    @Override
    public void updateSavedBusinesses(List<Business> businesses) {
        recyclerAdapter.update(businesses);
    }
}