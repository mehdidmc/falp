package ir.nobahar.falp.view.activities.main.fragments.meFragment.fragments.saves;

import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.JsonObject;


import ir.nobahar.falp.R;
import ir.nobahar.falp.model.businessSaved.BusinessSavedResponse;
import ir.nobahar.falp.util.Assistant;
import ir.nobahar.falp.util.JwtTokens;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.MainServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeSavesPresenter {

    private final Activity activity;
    private final MeSavesView view;

    public MeSavesPresenter(Activity activity, MeSavesView view) {
        this.activity = activity;
        this.view = view;
    }

    public void getSavedBusinesses(int startItem) {
        String jwt = JwtTokens.getInstance(activity).getJwtTokens();
        String decodedJwt = Assistant.get().decodeJwt(jwt);
        JsonObject jsonObject = new Gson().fromJson(decodedJwt, JsonObject.class);
        String hashId = jsonObject.getAsJsonObject("data").get("id").getAsString();

        MainServices services = RetrofitInstance.getInstance().create(MainServices.class);
        Call<BusinessSavedResponse> call = services.getSavedBusiness(jwt, hashId, startItem, 5);
        call.enqueue(new Callback<BusinessSavedResponse>() {
            @Override
            public void onResponse(Call<BusinessSavedResponse> call, Response<BusinessSavedResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    view.updateSavedBusinesses(response.body().getBusinesses());
                }
            }

            @Override
            public void onFailure(Call<BusinessSavedResponse> call, Throwable t) {
                view.showMessage(activity.getString(R.string.retrofit_onFailure));
                t.printStackTrace();
            }
        });
    }

    public int updateRecyclerViewItem(int lastStartItem) {
        int currentStartItem = lastStartItem + 5;
        getSavedBusinesses(currentStartItem);
        return currentStartItem;
    }
}
