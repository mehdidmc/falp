package ir.nobahar.falp.view.activities.businessDetail.fragments.businessImages.adapter.galleryFilters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.BusinessImagesFiltersItemsLayoutBinding;
import ir.nobahar.falp.model.businessDetail.galleryImages.GalleryFilters;

public class BusinessGalleryFiltersAdapter extends RecyclerView.Adapter<BusinessGalleryFiltersViewHolder> {

    private final Context context;
    private final List<GalleryFilters> filters;

    public BusinessGalleryFiltersAdapter(Context context, List<GalleryFilters> filters) {
        this.context = context;
        this.filters = filters;
    }

    @NonNull
    @Override
    public BusinessGalleryFiltersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        BusinessImagesFiltersItemsLayoutBinding bind = DataBindingUtil.inflate(inflater, R.layout.business_images_filters_items_layout, parent, false);
        return new BusinessGalleryFiltersViewHolder(bind);
    }

    @Override
    public void onBindViewHolder(@NonNull BusinessGalleryFiltersViewHolder holder, int position) {
        GalleryFilters filters = this.filters.get(position);
        holder.bind.setFilter(filters);
    }

    @Override
    public int getItemCount() {
        if (filters.isEmpty()) return 0;
        else return filters.size();
    }

    public void update(List<GalleryFilters> filters) {
        this.filters.addAll(filters);
        notifyDataSetChanged();
    }
}
