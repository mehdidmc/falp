package ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation;

import android.app.Activity;

import org.jetbrains.annotations.NotNull;

import ir.nobahar.falp.R;
import ir.nobahar.falp.model.business.Business;
import ir.nobahar.falp.model.businessDetail.BusinessInfoResponse;
import ir.nobahar.falp.util.JwtTokens;
import ir.nobahar.falp.webServices.RetrofitInstance;
import ir.nobahar.falp.webServices.services.BusinessDetailServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessInfoPresenter {

    private final Activity activity;
    private final BusinessInfoView view;

    public BusinessInfoPresenter(Activity activity, BusinessInfoView view) {
        this.activity = activity;
        this.view = view;
    }

    public void getBusinessInfo(String url, String userId, String bizId) {
        String jwtToken = JwtTokens.getInstance(activity).getJwtTokens();

        BusinessDetailServices services = RetrofitInstance.getInstance().create(BusinessDetailServices.class);
        Call<BusinessInfoResponse> call = services.getBusinessInfo(jwtToken, url, userId, bizId);
        call.enqueue(new Callback<BusinessInfoResponse>() {
            @Override
            public void onResponse(@NotNull Call<BusinessInfoResponse> call, @NotNull Response<BusinessInfoResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Business business = response.body().getBusinesses().get(0);
                    view.setInfo(business);
                }
            }

            @Override
            public void onFailure(@NotNull Call<BusinessInfoResponse> call, @NotNull Throwable t) {
                view.showMessage(activity.getResources().getString(R.string.retrofit_onFailure));
                t.printStackTrace();
            }
        });
    }
}
