package ir.nobahar.falp.view.activities.userAccount;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import ir.nobahar.falp.R;
import ir.nobahar.falp.databinding.ActivityUserAccountBinding;

public class UserAccountActivity extends AppCompatActivity {

    private ActivityUserAccountBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_account);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_account);
    }
}