package ir.nobahar.falp.view.activities.businessDetail.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import ir.nobahar.falp.R;
import ir.nobahar.falp.view.activities.businessDetail.fragments.businessComments.BusinessCommentsFragment;
import ir.nobahar.falp.view.activities.businessDetail.fragments.businessFullView.BusinessFullViewFragment;
import ir.nobahar.falp.view.activities.businessDetail.fragments.businessImages.BusinessImagesFragment;
import ir.nobahar.falp.view.activities.businessDetail.fragments.businessInformation.BusinessInformationFragment;
import ir.nobahar.falp.model.business.Business;

public class BusinessTabAdapter extends FragmentStateAdapter {

    private final String[] tabTitles;
    private final Business business;

    public BusinessTabAdapter(@NonNull FragmentActivity fragmentActivity, Context context, Business business) {
        super(fragmentActivity);
        tabTitles = context.getResources().getStringArray(R.array.business_tab_title);
        this.business = business;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0: return new BusinessFullViewFragment(business);
            case 1: return new BusinessInformationFragment(business);
            case 2: return new BusinessImagesFragment(business);
            default:return new BusinessCommentsFragment(business);
        }
    }

    @Override
    public int getItemCount() {
        return tabTitles.length;
    }
}
